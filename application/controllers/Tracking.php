<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_tracking','mp');
		$this->load->model('admin/m_kategori','mk');
	}

	public function index()
	{

		$kat = $this->input->post('kat');

		$data = array(
			'book' => $this->mp->getAll(),
			'data'=> $this->mp->getStok($kat),
			'kategori' => $this->mk->getKategori()
		);
		$this->load->view('tracking',$data);
	}

	public function getBuku()
	{
			$a = $this->input->post('id_buku');
			$buku = $this->mp->getIdBuku($a);

			echo json_encode($buku);
	}


}

/* End of file Tracking.php */
/* Location: ./application/controllers/Tracking.php */