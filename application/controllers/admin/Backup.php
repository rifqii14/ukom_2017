<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

	public function __construct()
	{
	    parent::__construct(); 
	    $this->general->cekAdminLogin();
	}

	public function index()
	{	

		// $prefs = array(
		//         'add_drop'   => TRUE, 
		//         'add_insert' => TRUE,
		//         'newline'    => "\n"
		// );

		// $this->load->dbutil($prefs);

		// $nmfile = "141510300_ukom_datetime_".date('Y-m-d-H:i:s').'.sql.zip';
		// $backup = $this->dbutil->backup();
		// write_file('application\controllers\admin\backup/'.$nmfile, $backup);
		// force_download($nmfile, $backup);

		$this->load->dbutil();
		
		$prefs = array(
		  'format' => 'zip',
		  'filename' => 'ukom2017_141510300.sql'
		);
		
		$backup =& $this->dbutil->backup($prefs);
		
		$db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.zip'; // file name
		$save  = 'backup/db/' . $db_name; // dir name backup output destination
		
		$this->load->helper('file');
		write_file($save, $backup);
		
		$this->load->helper('download');
		force_download($db_name, $backup);
	}

}

/* End of file Backup.php */
/* Location: ./application/controllers/admin/Backup.php */