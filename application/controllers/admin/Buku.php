<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->_module = 'admin';
		$this->load->model('admin/m_buku','mb');
		$this->load->model('admin/m_kategori','mk');
		$this->load->model('admin/m_diskon','md');

	}

	public function index()
	{
		$data = array(
		'data' => $this->mb->getBuku(),
		'kategori' => $this->mk->getKategori(),
		'diskon' => $this->md->getDiskon(),
		'cek' => $this->mb->cekStok()
		);
		$this->load->view($this->_module.'/buku/v_buku',$data);
	}

	public function form()
	{
		$data = array(
		'kategori' => $this->mk->getKategori(),
		'diskon' => $this->md->getDiskon()
		);
		$this->load->view($this->_module.'/buku/add',$data);
	}

	public function save() 
	{

		//Upload properties
		$nmfile = "file_".time();
		$config['upload_path'] = 'assets/uploads/cover';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|JPG|PNG';
		$config['max_size'] = '10240';
		$config['max_width']  = '5000';
		$config['max_height']  = '5000';
		$config['file_name'] = $nmfile;

		$this->upload->initialize($config);
		if($_FILES['filefoto']['name'])
		{
		    if ($this->upload->do_upload('filefoto'))
		    {
		        $gbr = $this->upload->data();
		        $data = array(
		            'id_buku' => $this->input->post('val-id-buku'),
		            'id_kategori' => $this->input->post('val-kategori'),
		            'judul' => $this->input->post('val-judul'),
		            'cover' => $gbr['file_name'],
		            'noisbn' => $this->input->post('val-noisbn'),
		            'penulis' => $this->input->post('val-penulis'),
		            'penerbit' => $this->input->post('val-penerbit'),
		            'tahun' => $this->input->post('val-tahun'),
		            // 'stok' => $this->input->post('val-stok'),
		            'harga_pokok' => $this->input->post('val-pokok'),
		            'harga_jual' => $this->input->post('val-pokok')*(1+$this->input->post('val-ppn')/100)*$this->input->post('val-diskon'),
		            'ppn' => $this->input->post('val-ppn')/100,
		            'diskon' => $this->input->post('val-diskon'),
		            'create' => date('Y-m-d H:i:s')
		        );

				//Resize picture
		        
		        $this->mb->create($data);
		        $config2['image_library'] = 'gd2'; 
		        $config2['source_image'] = 'assets/uploads/cover/'.$this->upload->file_name;
		        $config2['new_image'] = 'assets/hasil_resize/cover/';
		        $config2['maintain_ratio'] = TRUE;
		        $config2['width'] = 100; 
		        $config2['height'] = 100;
		        $this->load->library('image_lib',$config2); 

		        if ( !$this->image_lib->resize()){
		        $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));   
		      }
		
		        $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");
		        redirect('admin/Buku'); 
		    }else{
		        
		        $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Gagal upload gambar !!</div></div>");
		        redirect('admin/Distributor');
		    }
		}
	}

	public function remove() {
		$id = $this->uri->segment(4);
		$data = array(
				'id_buku'=> $this->uri->segment(4),
				'delete' => date('Y-m-d H:i:s')
		);
		$this->mb->delete($data,$id);
		redirect('admin/Buku');
	}

	public function modified() {
		$id = $this->input->post('val-id-buku');
		$data = array(
			'id_buku' => $this->input->post('val-id-buku'),
			'judul' => $this->input->post('val-judul'),
			'noisbn' => $this->input->post('val-noisbn'),
			'penulis' => $this->input->post('val-penulis'),
			'penerbit' => $this->input->post('val-penerbit'),
			'tahun' => $this->input->post('val-tahun'),
			'harga_pokok' => $this->input->post('val-pokok'),
			'harga_jual' => $this->input->post('val-pokok')*(1+$this->input->post('val-ppn')/100)*$this->input->post('val-diskon'),
			'ppn' => $this->input->post('val-ppn')/100,
			'diskon' => $this->input->post('val-diskon'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mb->update($data,$id);
		redirect('admin/Buku');
	}

}

/* End of file Buku.php */
/* Location: ./application/controllers/admin/Buku.php */