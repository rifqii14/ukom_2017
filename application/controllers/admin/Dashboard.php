<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->_module = 'admin';
		$this->load->model('admin/m_dashboard','md');
	}
	
	public function index()
	{

		$year1 = $this->md->getTahun();
		$year = $year1['tahun'];
		$earning1 = $this->md->getTotal($year);
		$earning2 = $this->md->getTotal2($year);

		$total1= array();
		$total2= array();


		foreach ($earning1 as $tot) {
			$total1[] = $tot->total_pasok;
		}
		

		$bulan = $this->md->getBulan();
		$label = array();
		foreach ($bulan as $m) {
			$label[] = $m->bulan;
		}

		// batas

		foreach ($earning2 as $tot) {
			$total2[] = $tot->keuntungan;
		}
		

		$bulan2 = $this->md->getBulan2();
		$label2 = array();
		foreach ($bulan2 as $m) {
			$label2[] = $m->bulan;
		}

		// batas

		$data = array(
		'jumlah_buku' => $this->md->hitungBuku()->num_rows(),
		'jumlah_distributor' => $this->md->hitungDis()->num_rows(),
		'untung' => $this->md->hitungUntung(),
		'untung_hari' => $this->md->hitungUntungHari()
		);

		$data['label'] = json_encode($label);
		$data['tahun'] = $this->md->getTahun();
		$data['result1'] = json_encode($total1);
		$data['one'] = $year1['tahun'];

		$data['label2'] = json_encode($label2);
		$data['tahun2'] = $this->md->getTahun2();
		$data['result2'] = json_encode($total2);
		$data['one2'] = $year1['tahun'];

		$this->load->view('admin/dashboard',$data);	
	}
}

/* End of file c_adashboard.php */
/* Location: ./application/controllers/admin/c_adashboard.php */