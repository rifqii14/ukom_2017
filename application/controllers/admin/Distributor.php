<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->_module = 'admin';
		$this->load->model('admin/m_distributor','mdb');
	}

	public function index()
	{
		$data = array(
		'data' => $this->mdb->getDistributor()
		);
		$this->load->view($this->_module.'/distributor/v_distributor',$data);
	}

	public function form() 
	{
		$this->load->view($this->_module.'/distributor/add');
	}

	public function save() 
	{
		$data = array(
		    'id_distributor' => $this->input->post('val-id-distributor'),
		    'nama_distributor' => $this->input->post('val-nama-distributor'),
		    'alamat' => $this->input->post('val-alamat'),
		    'telepon' => $this->input->post('val-telepon'),
		    'create' => date('Y-m-d H:i:s')
		);
		$this->mdb->create($data);
		redirect('admin/Distributor');
	}

	public function remove() {
		$id = $this->uri->segment(4);
		$data = array(
				'id_distributor'=> $this->uri->segment(4),
				'delete' => date('Y-m-d H:i:s')
		);
		$this->mdb->delete($data,$id);
		redirect('admin/Distributor');
	}

	public function modified() {
		$id = $this->input->post('val-id-distributor');
		$data = array(
			'id_distributor' => $this->input->post('val-id-distributor'),
			'nama_distributor' => $this->input->post('val-nama-distributor'),
			'alamat' => $this->input->post('val-alamat'),
			'telepon' => $this->input->post('val-telepon'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mdb->update($data,$id);
		redirect('admin/Distributor');
	}
}

/* End of file Distributor.php */
/* Location: ./application/controllers/admin/Distributor.php */