<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pasok extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->_module = 'admin';
		$this->load->model('admin/m_pasok','mp');
		$this->load->model('admin/m_buku','mb');
		$this->load->model('admin/m_distributor','mdb');
	}

	public function index()
	{
		$data = array(
		'data' => $this->mp->getPasok()
		);
		$this->load->view($this->_module.'/pasok/v_pasok',$data);
	}

	public function form()
	{
		$data = array(
		'buku' => $this->mb->getBuku(),
		'distributor' => $this->mdb->getDistributor()
		);
		$this->load->view($this->_module.'/pasok/add',$data);
	}

	public function save() 
	{
		// $data = array(
		//     'id_pasok' => $this->input->post('val-id-pasok'),
		//     'id_distributor' => $this->input->post('val-nama-distributor'),
		//     'id_buku' => $this->input->post('val-judul'),
		//     'jumlah' => $this->input->post('val-jumlah'),
		//     'tanggal' => date('Y-m-d H:i:s'),
		//     'create' => date('Y-m-d H:i:s')
		// );
		// $this->mp->create($data);
		// redirect('admin/Pasok');

		$this->db->trans_begin();
		$data = json_decode($this->input->post('kirim'));

		foreach($data as $row) {
		    $filter_data = array(
		    	    // 'id_pasok' => $this->input->post('val-id-pasok'),
		    	    'id_distributor' => $row->id_distributor,
		    	    'id_buku' => $row->id_buku,
		    	    'jumlah' => $row->jumlah,
		    	    'tanggal' => date('Y-m-d H:i:s'),
		    	    'create' => date('Y-m-d H:i:s')
		    );
		   //Call the save method
		   $this->mp->create($filter_data);
		}


		if ($this->db->trans_status() === FALSE) {
		    $this->db->trans_rollback();
		    echo json_encode("Failed to Save Data");
		    // echo json_encode("<script>alert('Gagal ');window.location.href='".base_url()."kasir/Penjualan'; </script>");   
		} else {
		    $this->db->trans_commit();
		    echo json_encode("Success!");
		    // echo "<script> alert('Berhasil');window.location.href='".base_url()."kasir/Order'; </script>";   
		}
		$this->db->trans_complete();

	}

	public function modified() {
		$id = $this->input->post('val-id-pasok');
		$data = array(
		    'id_pasok' => $this->input->post('val-id-pasok'),
		    'jumlah' => $this->input->post('val-jumlah'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mp->update($data,$id);
		redirect('admin/Pasok');
	}

}

/* End of file Pasok.php */
/* Location: ./application/controllers/admin/Pasok.php */