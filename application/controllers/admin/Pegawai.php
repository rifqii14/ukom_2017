<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->_module = 'admin';
		$this->load->model('admin/m_kasir','mks');
	}

	public function index()
	{
		$data = array(
		'data' => $this->mks->getKasir()
		);
		$this->load->view($this->_module.'/kasir/v_kasir',$data);
	}

	public function form() 
	{
		$this->load->view($this->_module.'/kasir/add');
	}

	public function save() 
	{
		$data = array(
		    'id_kasir' => $this->input->post('val-id-kasir'),
		    'nama' => $this->input->post('val-nama-kasir'),
		    'alamat' => $this->input->post('val-alamat'),
		    'telepon' => $this->input->post('val-telepon'),
		    'status' => $this->input->post('val-status'),
		    'username' => $this->input->post('val-username'),
		    'password' => md5($this->input->post('val-password')),
		    'akses' => $this->input->post('val-akses'),
		    'create' => date('Y-m-d H:i:s')
		);
		$this->mks->create($data);
		redirect('admin/Pegawai');
	}

	public function remove() {
		$id = $this->uri->segment(4);
		$data = array(
				'id_kasir'=> $this->uri->segment(4),
				'delete' => date('Y-m-d H:i:s')
		);
		$this->mks->delete($data,$id);
		redirect('admin/Pegawai');
	}

	public function modified() {
		$id = $this->input->post('val-id-kasir');
		$data = array(
			'id_kasir' => $this->input->post('val-id-kasir'),
			'nama' => $this->input->post('val-nama-kasir'),
			'alamat' => $this->input->post('val-alamat'),
			'telepon' => $this->input->post('val-telepon'),
			'status' => $this->input->post('val-status'),
			'username' => $this->input->post('val-username'),
			'akses' => $this->input->post('val-akses'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mks->update($data,$id);
		redirect('admin/Pegawai');
	}

}

/* End of file Kasir.php */
/* Location: ./application/controllers/admin/Pegawai.php */