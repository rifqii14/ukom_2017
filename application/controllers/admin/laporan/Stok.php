<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	public function __construct()
	{
	    parent::__construct(); 
	    $this->general->cekAdminLogin();
	    $this->_module = 'admin';
	    $this->load->model($this->_module.'/laporan/m_stok','pb');
	    $this->load->model($this->_module.'/m_kategori','mk');
	}
	
	public function index()
	{
		$kat = $this->input->post('kat');

		$data = array(
			'data'=> $this->pb->getStok($kat),
			'kategori' => $this->mk->getKategori()
		);

		$this->load->view($this->_module.'/laporan/v_stok', $data);

	}

}

/* End of file Stok.php */
/* Location: ./application/controllers/admin/laporan/Stok.php */