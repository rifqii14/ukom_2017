<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Untungperbulan extends CI_Controller {

	public function __construct()
	{
	    parent::__construct(); 
	    $this->general->cekAdminLogin();
	    $this->_module = 'admin';
	    $this->load->model($this->_module.'/laporan/m_untungperbulan','pb');
	}
	
	public function index()
	{
		$year1 = $this->input->post('tahun');

		$data = array(
			'data'=> $this->pb->getProfit($year1),
			'tahun'=> $year1
		);

		$this->load->view($this->_module.'/laporan/v_untungperbulan', $data);

	}

}

/* End of file Untungperbulan.php */
/* Location: ./application/controllers/admin/laporan/Untungperbulan.php */