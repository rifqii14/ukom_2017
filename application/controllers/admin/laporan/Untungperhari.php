<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Untungperhari extends CI_Controller {

	public function __construct()
	{
	    parent::__construct(); 
	    $this->general->cekAdminLogin();
	    $this->_module = 'admin';
	    $this->load->model($this->_module.'/laporan/m_untungperhari','mtp');
	}
	
	public function index()
	{
		$year1 = $this->input->post('tahun');
		$month1 = $this->input->post('bulan');

		$data = array(
			'data'=> $this->mtp->getProfit($year1,$month1),
			'tahun'=> $year1,
			'bulan'=> $month1
		);

		$this->load->view($this->_module.'/laporan/v_untungperhari', $data);

	}

}

/* End of file Untungperhari.php */
/* Location: ./application/controllers/admin/laporan/Untungperhari.php */