<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekKasirLogin();
		$this->_module = 'kasir';
		$this->load->model('kasir/m_order','mor');
		$this->load->model('kasir/m_order_detail','mod');
	}

	public function index()
	{
		$id_kasir =  $this->session->userdata('id_kasir');
		$data = array(
		'data' => $this->mor->getOrder($id_kasir)
		);
		$this->load->view($this->_module.'/order/v_order',$data);
	}

	function detail($id) {
		// $data = array(
		// 	'order' => $this->mor->getById($id),
		// 	'orderDetails' => $this->mod->getByOrderId($data['order']['id_penjualan_detail'])
		// );
		// 	// );
	    $data['order'] = $this->mor->getById($id);
	    $data['orderDetails'] = $this->mod->getByOrderId($data['order']['id_penjualan']);
	    $this->load->view($this->_module.'/order/v_order_detail',$data);
	}


}

/* End of file Order.php */
/* Location: ./application/controllers/kasir/Order.php */