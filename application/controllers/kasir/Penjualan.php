<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->general->cekKasirLogin();
		$this->_module = 'kasir';
		$this->load->model('kasir/m_penjualan','mp');
	}

	public function index()
	{
		$data = array(
			'cek' => $this->mp->getBuku()
			);
		$this->load->view($this->_module.'/penjualan/v_penjualan');
	}


	public function getBuku()
	{
			$a = $this->input->post('noisbn');
			$buku = $this->mp->getbyNumberISBN($a);

			echo json_encode($buku);
	}

	public function simpan()
	{
		$numberRef = $this->general->generateRandomCode(8);

		$this->db->trans_begin();

		$master['no_referensi'] = $numberRef;
		$master['id_kasir'] = $this->session->userdata('id_kasir');
		$master['tanggal'] = date('Y-m-d H:i:s');
		$master['create'] = date('Y-m-d H:i:s');
		$this->mp->simpanPenjualan($master);
		$table1_id=$this->db->insert_id();

		

		$data = json_decode($this->input->post('kirim'));
		foreach($data as $row) {
		    $filter_data = array(
		    	"id_penjualan" =>$table1_id,
		        "id_buku" => $row->id_buku,
		        "jumlah" => $row->jumlah,
		        "subtotal" => $row->subtotal,
		        'create' => date('Y-m-d H:i:s')
		    );
		   //Call the save method
		   $this->mp->simpanPenjualanDetail($filter_data);
		}


		if ($this->db->trans_status() === FALSE) {
		    $this->db->trans_rollback();
		    echo json_encode("Failed to Save Data");
		    // echo json_encode("<script>alert('Gagal ');window.location.href='".base_url()."kasir/Penjualan'; </script>");   
		} else {
		    $this->db->trans_commit();
		    echo json_encode("Success!");
		    // echo "<script> alert('Berhasil');window.location.href='".base_url()."kasir/Order'; </script>";   
		}
		$this->db->trans_complete();
	}

}

/* End of file Penjualan.php */
/* Location: ./application/controllers/kasir/Penjualan.php */