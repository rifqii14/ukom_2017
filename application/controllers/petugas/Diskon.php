<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Diskon extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekPetugasLogin();
		$this->_module = 'petugas';
		$this->load->model('petugas/m_diskon','md');
	}

	public function index()
	{
		$data = array(
		'data' => $this->md->getDiskon()
		);
		$this->load->view($this->_module.'/diskon/v_diskon',$data);
	}

	public function form() 
	{
		$this->load->view($this->_module.'/diskon/add');
	}


	public function save() 
	{
		$data = array(
		    'diskon' => 1-$this->input->post('val-diskon')/100,
		    'create' => date('Y-m-d H:i:s')
		);
		$this->md->create($data);
		redirect('petugas/Diskon');
	}

}

/* End of file Diskon.php */
/* Location: ./application/controllers/petugas/Diskon.php */