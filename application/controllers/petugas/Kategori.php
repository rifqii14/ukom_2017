<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekPetugasLogin();
		$this->_module = 'petugas';
		$this->load->model('petugas/m_kategori','mk');
	}

	public function index()
	{
		$data = array(
		'data' => $this->mk->getKategori()
		);
		$this->load->view($this->_module.'/kategori/v_kategori',$data);
	}

	public function form() 
	{
		$this->load->view($this->_module.'/kategori/add');
	}

	public function save() 
	{
		$data = array(
		    'id_kategori' => $this->input->post('val-id-kategori'),
		    'kategori' => $this->input->post('val-kategori'),
		    'create' => date('Y-m-d H:i:s')
		);
		$this->mk->create($data);
		redirect('petugas/Kategori');
	}

	public function remove() {
		$id = $this->uri->segment(4);
		$data = array(
				'id_kategori'=> $this->uri->segment(4),
				'delete' => date('Y-m-d H:i:s')
		);
		$this->mk->delete($data,$id);
		redirect('petugas/Kategori');
	}

	public function modified() {
		$id = $this->input->post('val-id-kategori');
		$data = array(
		    'id_kategori' => $this->input->post('val-id-kategori'),
			'kategori' => $this->input->post('val-kategori'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mk->update($data,$id);
		redirect('petugas/Kategori');
	}

}

/* End of file Kategori.php */
/* Location: ./application/controllers/petugas/Kategori.php */