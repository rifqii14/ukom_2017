<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pasok extends CI_Controller {

	//Constructor to load library, module, model, or etc.
	public function __construct() 
	{
		parent::__construct();
		$this->general->cekPetugasLogin();
		$this->_module = 'petugas';
		$this->load->model('petugas/m_pasok','mp');
		$this->load->model('petugas/m_buku','mb');
		$this->load->model('petugas/m_distributor','mdb');
	}

	public function index()
	{
		$data = array(
		'data' => $this->mp->getPasok()
		);
		$this->load->view($this->_module.'/pasok/v_pasok',$data);
	}

	public function form()
	{
		$data = array(
		'buku' => $this->mb->getBuku(),
		'distributor' => $this->mdb->getDistributor()
		);
		$this->load->view($this->_module.'/pasok/add',$data);
	}

	public function save() 
	{
		$data = array(
		    'id_pasok' => $this->input->post('val-id-pasok'),
		    'id_distributor' => $this->input->post('val-nama-distributor'),
		    'id_buku' => $this->input->post('val-judul'),
		    'jumlah' => $this->input->post('val-jumlah'),
		    'tanggal' => date('Y-m-d H:i:s'),
		    'create' => date('Y-m-d H:i:s')
		);
		$this->mp->create($data);
		redirect('petugas/Pasok');
	}

	public function modified() {
		$id = $this->input->post('val-id-pasok');
		$data = array(
		    'id_pasok' => $this->input->post('val-id-pasok'),
		    'jumlah' => $this->input->post('val-jumlah'),
			'update' => date('Y-m-d H:i:s')
		);
		$this->mp->update($data,$id);
		redirect('petugas/Pasok');
	}

}

/* End of file Pasok.php */
/* Location: ./application/controllers/petugas/Pasok.php */