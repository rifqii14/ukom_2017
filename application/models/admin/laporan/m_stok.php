<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stok extends CI_Model {

	public function getStok($kat){
		$this->db->select('*');
		$query = $this->db->get_where('v_stok_buku', array('kategori' => $kat));;
		return $query->result();
	}


}

/* End of file m_stok.php */
/* Location: ./application/models/admin/laporan/m_stok.php */