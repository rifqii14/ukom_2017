<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_untungperbulan extends CI_Model {

	    public function getProfit($year){
	    	$this->db->select('*');
	    	$this->db->order_by('no_bulan');
	    	$query = $this->db->get_where('v_profit_perbulan', array('tahun' => $year));;
	    	return $query->result();
	    }

}

/* End of file m_untungperbulan.php */
/* Location: ./application/models/admin/laporan/m_untungperbulan.php */