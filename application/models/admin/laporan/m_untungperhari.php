<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_untungperhari extends CI_Model {

	public function getProfit($year,$month){
		$this->db->select('*');
		$query = $this->db->get_where('v_profit_perhari', array('tahun' => $year , 'bulan'=>$month));;
		return $query->result();
	}

}

/* End of file m_untungperhari.php */
/* Location: ./application/models/admin/laporan/m_untungperhari.php */