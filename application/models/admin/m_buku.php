<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_buku extends CI_Model {

	 public function getBuku() 
	 {
	 	
	   $this->db->select('id_buku, kategori, judul, cover, noisbn, penulis, penerbit, tahun, stok, harga_pokok, harga_jual, ppn, tbl_buku.diskon');
	   $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_buku.id_kategori', 'inner');
	   $this->db->join('tbl_diskon', 'tbl_diskon.diskon = tbl_buku.diskon', 'left');
	   $query = $this->db->get_where('tbl_buku', array('tbl_buku.delete' => NULL));
	   return $query->result_array();

	}

	public function cekStok()
	{
		$query = $this->db->get_where('tbl_buku', array('delete' => NULL , 'stok <' => '3'));
		return $query->result_array();
	}

	public function create($data)
	{

	   $this->db->insert('tbl_buku', $data);
	   return TRUE;

	}

	public function delete($data,$id)
	{
		$this->db->where('id_buku',$id);
		$this->db->update('tbl_buku',$data);
		return TRUE;
	}

	public function update($data,$id) 
	{
	   $this->db->where('id_buku',$id);
	   $this->db->update('tbl_buku',$data);
	   return TRUE;
	}


}

/* End of file m_buku.php */
/* Location: ./application/models/admin/m_buku.php */