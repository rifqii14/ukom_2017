<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_diskon extends CI_Model {

	public function getDiskon() 
	{
	   $this->db->order_by('diskon','desc');
	   $query = $this->db->get('tbl_diskon');
	   return $query->result_array();

	}

	public function create($data)
	{

	   $this->db->insert('tbl_diskon', $data);
	   return TRUE;

	}


}

/* End of file m_diskon.php */
/* Location: ./application/models/admin/m_diskon.php */