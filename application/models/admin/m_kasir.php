<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_kasir extends CI_Model {

	public function getKasir() 
	{
		$this->db->order_by('akses', 'ASC');
	   	$query = $this->db->get_where('tbl_kasir', array('tbl_kasir.delete' => NULL));
	   	return $query->result_array();

	}	

	public function create($data)
	{

	   $this->db->insert('tbl_kasir', $data);
	   return TRUE;

	}

	public function delete($data,$id)
	{
		$this->db->where('id_kasir',$id);
		$this->db->update('tbl_kasir',$data);
		return TRUE;
	}

	public function update($data,$id) 
	{
	   $this->db->where('id_kasir',$id);
	   $this->db->update('tbl_kasir',$data);
	   return TRUE;
	}

}

/* End of file m_kasir.php */
/* Location: ./application/models/admin/m_kasir.php */
