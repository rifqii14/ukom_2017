<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends CI_Model {

	public function getMember()
	{
		$query = $this->db->get_where('tbl_user', array('delete' => NULL));
		return $query->result_array();
	}

	public function update($data,$id) 
	{
	   $this->db->where('id_user',$id);
	   $this->db->update('tbl_user',$data);
	   return TRUE;
	}

}

/* End of file m_member.php */
/* Location: ./application/models/admin/m_member.php */