<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_pasok extends CI_Model {

	public function getPasok() 
	{

	 	$this->db->select('id_pasok,nama_distributor, judul, jumlah, tanggal, tbl_pasok.create, tbl_pasok.update');
	 	$this->db->join('tbl_buku', 'tbl_buku.id_buku = tbl_pasok.id_buku', 'inner');
	 	$this->db->join('tbl_distributor', 'tbl_distributor.id_distributor = tbl_pasok.id_distributor', 'inner');
	 	$this->db->order_by('tanggal','desc');
	   	$query = $this->db->get_where('tbl_pasok', array('tbl_pasok.delete' => NULL));
	   	return $query->result_array();

	}

	public function create($data)
	{

	   $this->db->insert('tbl_pasok', $data);
	   return TRUE;

	}

	public function update($data,$id) 
	{
	   $this->db->where('id_pasok',$id);
	   $this->db->update('tbl_pasok',$data);
	   return TRUE;
	}

}

/* End of file m_pasok.php */
/* Location: ./application/models/admin/m_pasok.php */