<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_order extends CI_Model {

	var $table = 'v_penjualan';

	public function getOrder($id_kasir) 
	{

	   $this->db->select('*');
	   $this->db->order_by('tanggal', 'DESC');
	   $query = $this->db->get_where('v_penjualan', array('v_penjualan.delete' => NULL, 'v_penjualan.id_kasir' => $id_kasir));
	   return $query->result_array();

	}

	public function getById($id) {
	    $this->db->where('id_penjualan', $id);
	    $query = $this->db->get($this->table, 1);
	    if ($query->num_rows() == 1) {
	        return $query->row_array();
	    }
	}

}

/* End of file m_order.php */
/* Location: ./application/models/kasir/m_order.php */