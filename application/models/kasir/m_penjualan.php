<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_penjualan extends CI_Model {

	public function getBuku()
	{

		$query = $this->db->get_where('tbl_buku', array('delete' => NULL));
		return $query->result_array();

	}

	public function getbyNumberISBN($a)
	{
		$query = $this->db->get_where('tbl_buku', array('noisbn' => $a , 'delete' => NULL));
		return $query->row();
	}

	public function simpanPenjualan($data){
		$this->db->insert('tbl_penjualan', $data);
	}

	public function simpanPenjualanDetail($data) {
	    $this->db->insert('tbl_penjualan_detail', $data);
	}
}

/* End of file m_penjualan.php */
/* Location: ./application/models/kasir/m_penjualan.php */