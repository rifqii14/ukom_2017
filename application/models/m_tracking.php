<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tracking extends CI_Model {

	public function getAll()
	{
		$query = $this->db->get_where('tbl_buku', array('tbl_buku.delete' => NULL));
		return $query->result_array();

	}

	public function getIdBuku($a)
	{
		$this->db->select('id_buku,tbl_kategori.id_kategori,judul,kategori,harga_jual,stok');
		$this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_buku.id_kategori', 'inner');
		$query = $this->db->get_where('tbl_buku', array('id_buku' => $a , 'tbl_buku.delete' => NULL));
		return $query->row();
	}

	public function getStok($kat){
		$this->db->select('*');
		$query = $this->db->get_where('v_stok_buku', array('kategori' => $kat));;
		return $query->result();
	}



}

/* End of file m_tracking.php */
/* Location: ./application/models/m_tracking.php */