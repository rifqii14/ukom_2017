<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function hitungBuku()
	{	
		$this->db->select('tbl_buku.id_buku');
		$this->db->from('tbl_buku');
		$this->db->where('delete',null);
	   	return $this->db->get();
	}

	public function hitungDis()
	{	
		$this->db->select('tbl_distributor.id_distributor');
		$this->db->from('tbl_distributor');
		$this->db->where('delete',null);
	   	return $this->db->get();
	}

	public function hitungUntung()
	{	
		$this->db->select('*');
		$this->db->from('v_profit_perbulan');
		$this->db->where('no_bulan=month(now())');
		$this->db->where('tahun=year(now())');
	   	// return $this->db->get();

		$retr = array();
	   	$retr = $this->db->get()->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'no_bulan' => $retr->no_bulan,
	   			'bulan' => $this->general->humanDate3(date('F, Y')),
	   			'tahun' => $retr->tahun,
	   			'keuntungan' => $retr->keuntungan
	   			);
	   	}
	   	else{$ret = array(
	   			'no_bulan' => date('m'),
	   			'bulan' => $this->general->humanDate3(date('F, Y')),
	   			'tahun' => date('Y'),
	   			'keuntungan' => '0'
	   		);}

	   	return $ret;
	}

	public function hitungUntungHari()
	{	
		$this->db->select('*');
		$this->db->from('v_profit_perhari');
		$this->db->where('no_bulan=month(now())');
		$this->db->where('tanggal=day(now())');
		$this->db->where('tahun=year(now())');

		$retr = array();
	   	$retr = $this->db->get()->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'hari' => $retr->hari,
	   			'keuntungan' => $retr->keuntungan
	   			);
	   	}
	   	else{$ret = array(
	   			'hari' => date('Y-m-d'),
	   			'keuntungan' => '0'
	   		);}

	   	return $ret;

	}

	// BATAS

	    public function getTotalJSON($year) {
			$sql = "SELECT total_pasok FROM v_totalpasok WHERE tahun = $year ORDER BY no_bulan";
			$totals = array(); 

			while($res = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
				$totals[] = $res['total_pasok'];
			}

			$final = json_encode($totals);
			$results = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:', $final);
			print $results;
		}
	    
	    public function getChartLabelJSON() {
			$sql = "SELECT DISTINCT bulan FROM v_totalpasok ORDER BY no_bulan ASC";
			$months = array();

			while($res = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
				$months[] = $res['bulan'];
			}
			print json_encode($months);
		}
	    
		public function getTahun(){
			$this->db->select('tahun');
			$this->db->from('v_totalpasok');
			$this->db->where('tahun=year(now())');

			$retr = array();
			$retr = $this->db->get()->row();

			if ($retr != null ) {
				$ret = array(
					'tahun' => $retr->tahun
					);
			}
			else{$ret = array(
					'tahun' => date('Y')
				);}

			return $ret;
		}

	    public function getBulan(){
	        $this->db->select('bulan');
	        $this->db->order_by('no_bulan');
	        $query = $this->db->get('v_totalpasok');
	        return $query->result();
	    }

	    public function getTotal($year){
	    	$this->db->select('total_pasok');
	    	$this->db->order_by('no_bulan');
	    	$this->db->where('tahun',$year);
	    	$query = $this->db->get('v_totalpasok');
	    	return $query->result();
	    }

	    // =========================================================================================/

	        public function getTotalJSON2($year) {
	    		$sql = "SELECT keuntungan FROM v_profit_perbulan WHERE tahun = $year ORDER BY no_bulan";
	    		$totals = array(); 

	    		while($res = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
	    			$totals[] = $res['keuntungan'];
	    		}

	    		$final = json_encode($totals);
	    		$results = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:', $final);
	    		print $results;
	    	}
	        
	        public function getChartLabelJSON2() {
	    		$sql = "SELECT DISTINCT bulan FROM v_profit_perbulan ORDER BY no_bulan ASC";
	    		$months = array();

	    		while($res = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
	    			$months[] = $res['bulan'];
	    		}
	    		print json_encode($months);
	    	}

	    	public function getTahun2(){
	    		$this->db->select('tahun');
	    		$this->db->from('v_profit_perbulan');
	    		$this->db->where('tahun=year(now())');

	    		$retr = array();
	    		$retr = $this->db->get()->row();

	    		if ($retr != null ) {
	    			$ret = array(
	    				'tahun' => $retr->tahun
	    				);
	    		}
	    		else{$ret = array(
	    				'tahun' => date('Y')
	    			);}

	    		return $ret;
	    	}
	        

	        public function getBulan2(){
	            $this->db->select('bulan');
	            $this->db->order_by('no_bulan');
	            $query = $this->db->get('v_profit_perbulan');
	            return $query->result();
	        }

	        public function getTotal2($year){
	        	$this->db->select('keuntungan');
	        	$this->db->order_by('no_bulan');
	        	$this->db->where('tahun',$year);
	        	$query = $this->db->get('v_profit_perbulan');
	        	return $query->result();
	        }

}

/* End of file m_dashboard.php */
/* Location: ./application/models/petugas/m_dashboard.php */