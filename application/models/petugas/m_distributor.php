<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_distributor extends CI_Model {

	public function getDistributor() 
	{

	   	$query = $this->db->get_where('tbl_distributor', array('delete' => NULL));
	   	return $query->result_array();

	}	

	public function create($data)
	{

	   $this->db->insert('tbl_distributor', $data);
	   return TRUE;

	}

	public function delete($data,$id)
	{
		$this->db->where('id_distributor',$id);
		$this->db->update('tbl_distributor',$data);
		return TRUE;
	}

	public function update($data,$id) 
	{
	   $this->db->where('id_distributor',$id);
	   $this->db->update('tbl_distributor',$data);
	   return TRUE;
	}

}

/* End of file m_distributor.php */
/* Location: ./application/models/admin/m_distributor.php */