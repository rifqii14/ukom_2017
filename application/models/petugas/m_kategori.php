<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

	 public function getKategori() 
	 {

	   $query = $this->db->get_where('tbl_kategori', array('delete' => NULL));
	   return $query->result_array();

	}

	public function create($data)
	{

	   $this->db->insert('tbl_kategori', $data);
	   return TRUE;

	}

	public function delete($data,$id)
	{
		$this->db->where('id_kategori',$id);
		$this->db->update('tbl_kategori',$data);
		return TRUE;
	}

	public function update($data,$id) 
	{
	   $this->db->where('id_kategori',$id);
	   $this->db->update('tbl_kategori',$data);
	   return TRUE;
	}

}

/* End of file m_kategori.php */
/* Location: ./application/models/admin/m_kategori.php */
?>