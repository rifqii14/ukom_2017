<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<?php 
foreach($cek as $q)
{   
echo "<div style='padding:5px' class='alert alert-warning'><span class='glyphicon glyphicon-info-sign'></span> Judul Buku <a style='color:red'>". $q['judul']."</a> kurang dari 3 stok. Silahkan tambah pasokan!</div>"; 
}
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Data Buku
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
        <p class="text-muted font-13 m-b-30">
          <a href="<?php echo site_url('admin/Buku/form') ?>">
          <button style="width:100px;" class="btn btn-success btn-block"><span class="icon-plus3"></span> Tambah</button>
          </a>
        </p>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th>Judul</th>
                        <th>Kategori</th>
                        <th>Cover</th>
                        <th>No. ISBN</th>
                        <th>Penulis</th>
                        <th>Penerbit & Tahun</th> 
                        <th>Stok</th>
                        <th>Harga Pokok</th>
                        <th>Harga Jual</th>
                        <th>PPn</th>
                        <th>Diskon</th>  
                        <th class="text-center" width="7%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                	<?php $no = 1; foreach($data as $b) { ?>
                    <tr>
                        <td class="text-center"><?php echo $no++?></td>
                        <td class="font-w600"><?php echo $b['judul'] ?></td>
                        <td><?php echo $b['kategori'] ?></td>
                        <td><img  src="<?php base_url()?>../assets/hasil_resize/cover/<?php echo $b['cover'];?>"></td>
                        <td><?php echo $b['noisbn'] ?></td>
                        <td><?php echo $b['penulis'] ?></td>
                        <td><?php echo $b['penerbit'].", ".$b['tahun'] ?></td>
                        <?php if($b['stok']>=3) { ?>
                            <td><span class="badge badge-success"><?php echo $b['stok'] ?></span></td>
                        <?php } else if($b['stok']>=1) { ?>
                            <td><span class="badge badge-warning"><?php echo $b['stok'] ?></span></td>
                        <?php } else if($b['stok']==0) { ?>
                            <td><span class="badge badge-danger"><?php echo $b['stok'] ?></span></td>
                        <?php }?>
                        <td><?php echo "Rp. ".number_format($b['harga_pokok']) ?></td>
                        <td><?php echo "Rp. ".number_format($b['harga_jual'])?></td>
                        <td><?php echo ($b['ppn'])*(100).'%' ?></td>
                        <td>
                        <?php 
                        if($b['diskon'] == true){
                            echo (1-$b['diskon'])*(100).'%' ;
                        }
                        else{
                            echo '0%' ;
                        }
                        
                        ?>        
                        </td>
                            
                        <td class="text-center">
                            <div class="btn-group">
                                <a 
                                href="javascript:;"
                                data-id-buku="<?php echo $b['id_buku'] ?>"
                                data-judul="<?php echo $b['judul'] ?>"
                                data-noisbn="<?php echo $b['noisbn'] ?>"
                                data-penulis="<?php echo $b['penulis'] ?>"
                                data-penerbit="<?php echo $b['penerbit'] ?>"
                                data-tahun="<?php echo $b['tahun'] ?>"
                                data-pokok="<?php echo $b['harga_pokok'] ?>"
                                data-ppn="<?php echo ($b['ppn'])*(100) ?>"
                                data-diskon="<?php echo (1-$b['diskon'])*(100)?>"
                                data-toggle="modal" data-target="#edit-data">
                                <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#ubah-data" title="Edit buku"><i class="fa fa-pencil"></i></button>
                                </a>
                                <a href="<?= base_url().'admin/Buku/remove/'.$b['id_buku']; ?>" class="hapus">
                                <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Hapus Buku"><i class="fa fa-times"></i></button>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Dynamic Table Full -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="edit-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Ubah Buku</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" action="<?php echo base_url('admin/Buku/modified')?>"  method="post" enctype="multipart/form-data" role="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Judul Buku</label>
                         <div class="col-lg-10">
                          <input type="hidden" id="val-id-buku" name="val-id-buku">
                             <input type="text" class="form-control" id="val-judul" name="val-judul" placeholder="Masukkan Judul Buku">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Nomor ISBN</label>
                         <div class="col-lg-5">
                             <input type="text" class="form-control" id="val-noisbn" name="val-noisbn" placeholder="Masukkan Nomor ISBN">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Penulis</label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-penulis" name="val-penulis" placeholder="Masukkan Penulis">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Penerbit</label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-penerbit" name="val-penerbit" placeholder="Masukkan Penerbit">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Tahun Terbit</label>
                         <div class="col-lg-3">
                             <input type="text" class="form-control" id="val-tahun" name="val-tahun" placeholder="Masukkan Tahun">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Harga Pokok</label>
                         <div class="col-lg-8">
                         <div class="input-group">
                             <span class="input-group-addon">Rp.</span>
                             <input class="form-control" type="text" id="val-pokok" name="val-pokok" placeholder="Masukkkan Harga Pokok">
                         </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">PPN</label>
                         <div class="col-lg-3">
                         <div class="input-group">
                         <input class="form-control" type="text" id="val-ppn" name="val-ppn" placeholder="Masukkan PPN" readonly>
                             <span class="input-group-addon">%</span>
                         </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Dsikon</label>
                         <div class="col-lg-10">
                         <?php
                         echo "
                         <select class='js-select2 form-control' name='val-diskon' id='val-diskon' style='width: 100%;' data-placeholder='Pilih Diskon'>
                          <option value='' disabled selected></option>";
                           foreach ($diskon as $d) {  
                           echo "<option value='".$d['diskon']."'>".(1-$d['diskon'])*(100).'%'."</option>";
                           }
                           echo"
                         </select>";
                         ?>
                         </div>
                     </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
        </div>
    </div>
<!-- End Modal Edit Data -->
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>

<!-- Page JS Code -->
<script>
    $('.hapus').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      swal({
          title: "Yakin Ingin Hapus?",
          text: "Data yang sudah dihapus tidak dapat dikembalikan!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn-danger",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            swal("Berhasil!", "Data berhasil dihapus!", "success");
            window.location.replace(url);
          } else {
            swal("Batal!", "Data tidak jadi terhapus!", "error");
          }
        });
    });

    //select2
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

    //daterpicker
    $("#val-tahun").datepicker( {
        format: " yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });

    //JQuery for Update Data
    $(document).ready(function() {
          $('#edit-data').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)

              modal.find('#val-id-buku').attr("value",div.data('id-buku'));
              modal.find('#val-judul').attr("value",div.data('judul'));
              modal.find('#val-noisbn').attr("value",div.data('noisbn'));
              modal.find('#val-penulis').attr("value",div.data('penulis'));
              modal.find('#val-penerbit').attr("value",div.data('penerbit'));
              modal.find('#val-tahun').attr("value",div.data('tahun'));
              modal.find('#val-pokok').attr("value",div.data('pokok'));
              modal.find('#val-ppn').attr("value",div.data('ppn'));
              modal.find('#val-diskon').attr("value",div.data('diskon'));
          });
      });
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>