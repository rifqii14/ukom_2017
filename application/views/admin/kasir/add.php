<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Tambah Kasir
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <!-- Bootstrap Forms Validation -->
            <div class="block">
                <div class="block-content block-content-narrow">
                    <base href="<?php echo base_url(); ?>" />
                    <?php echo form_open('admin/Pegawai/save',array('class' => 'js-validation-bootstrap form-horizontal'))?>
	                    <div class="form-group">
	                        <div class="col-md-4">
	                        <label class="control-label" for="val-nama-kasir">Nama <span class="text-danger">*</span></label>
	                        </div>
	                        <div class="col-md-8">
	                            <input type="hidden" id="val-id-kasir" name="val-id-kasir">
	                            <input class="form-control" type="text" id="val-nama-kasir" name="val-nama-kasir" placeholder="Masukkan Nama Kasir">
	                        </div>
	                    </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-alamat">Alamat<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="form-control" id="val-alamat" name="val-alamat" rows="18" placeholder="Masukkan Alamat"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-telepon">Nomor Telepon <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-telepon" name="val-telepon" placeholder="Masukkan Nomor Telepon">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-select2">Status <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <select class="js-select2 form-control" id="val-status" name="val-status" style="width: 100%;" data-placeholder="Pilih Status">
                                    <option value='' disabled selected></option>
                                    <option value="Aktif">Aktif</option>
                                    <option value="Tidak Aktif">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-username">Username <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-username" name="val-username" placeholder="Masukkan Username">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-password">Password <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-password" name="val-password" placeholder="Masukkan Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-confirm-password">Konfirmasi Password <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-confirm-password" name="val-confirm-password" placeholder="Masukkan Konfirmasi Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            <label class="control-label" for="val-akses">Akses <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <select class="js-select2 form-control" id="val-akses" name="val-akses" style="width: 100%;" data-placeholder="Pilih Akses">
                                    <option value='' disabled selected></option>
                                    <option value="Kasir">Kasir</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Petugas">Petugas</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- Bootstrap Forms Validation -->
        </div>
    </div>
    <!-- END Forms Row -->

</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>

<!-- Page JS Code -->
<script>
    //select2
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });
</script>

<?php
$this->load->view('layout/template_footer_end.php');
?>