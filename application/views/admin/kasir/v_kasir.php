<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Data Pegawai
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
        <p class="text-muted font-13 m-b-30">
          <a href="<?php echo site_url('admin/Pegawai/form') ?>">
          <button style="width:100px;" class="btn btn-success btn-block"><span class="icon-plus3"></span> Tambah</button>
          </a>
        </p>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Status</th>
                    <th>Username</th>
                    <th>Akses</th>
                    <th class="text-center" width="7%">Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php $no = 1; foreach($data as $p) { ?>
                <tr>
                    <td class="text-center"><?php echo $no++?></td>
                    <td class="font-w600" width="10%"><?php echo $p['nama'] ?></td>
                    <td width="10%"><?php echo $p['alamat'] ?></td>
                    <td><?php echo $p['telepon'] ?></td>
                    <?php if($p['status'] == 'Aktif'){?>
                        <td><span class="label label-success"><?php echo $p['status'] ?></span></td>
                    <?php }else{ ?>
                        <td><span class="label label-danger"><?php echo $p['status'] ?></span></td>
                    <?php } ?>
                    <td><?php echo $p['username'] ?></td>

                    <?php if($p['akses'] == 'Kasir'){?>
                        <td><span class="label label-info"><?php echo $p['akses'] ?></span></td>
                    <?php }else if($p['akses'] == 'Admin'){ ?>
                        <td><span class="label label-primary"><?php echo $p['akses'] ?></span></td>
                    <?php } else {?>
                        <td><span class="label label-default"><?php echo $p['akses'] ?></span></td>
                    <?php } ?>
                    <td class="text-center">
                        <div class="btn-group">
                            <a 
                            href="javascript:;"
                            data-id-kasir="<?php echo $p['id_kasir'] ?>"
                            data-nama-kasir="<?php echo $p['nama'] ?>"
                            data-alamat="<?php echo $p['alamat'] ?>"
                            data-telepon="<?php echo $p['telepon'] ?>"
                            data-status="<?php echo $p['status'] ?>"
                            data-username="<?php echo $p['username'] ?>"
                            data-akses="<?php echo $p['akses'] ?>"
                            data-toggle="modal" data-target="#edit-data">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#ubah-data" title="Edit Pegawai"><i class="fa fa-pencil"></i></button>
                            </a>
                            <a href="<?= base_url().'admin/Pegawai/remove/'.$p['id_kasir']; ?>" class="hapus">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Hapus Pegawai"><i class="fa fa-times"></i></button>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
<!-- END Dynamic Table Full -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="edit-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Ubah Pegawai</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" action="<?php echo base_url('admin/Pegawai/modified')?>"  method="post" enctype="multipart/form-data" role="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                         <div class="col-lg-10">
                          <input type="hidden" id="val-id-kasir" name="val-id-kasir">
                             <input type="text" class="form-control" id="val-nama-kasir" name="val-nama-kasir" placeholder="Masukkan Nama kasir">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Alamat </label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-alamat" name="val-alamat" placeholder="Masukkan Alamat kasir">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Nomor Telepon</label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-telepon" name="val-telepon" placeholder="Masukkan Nomor Telepon kasir">
                         </div>
                     </div>

                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Akses</label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-akses" name="val-akses" placeholder="Masukkan Akses" readonly>
                         </div>
                     </div>

                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Username</label>
                         <div class="col-lg-10">
                             <input type="text" class="form-control" id="val-username" name="val-username" placeholder="Masukkan Nomor Telepon kasir" readonly>
                         </div>
                     </div>

                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Status</label>
                         <div class="col-lg-10">
                                  <select class="js-select2 form-control" id="val-status" name="val-status" style="width: 100%;" data-placeholder="Pilih Status">
                                    <option value='' disabled selected></option>
                                    <option value="Aktif">Aktif</option>
                                    <option value="Tidak Aktif">Tidak Aktif</option>
                                </select>
                         </div>
                     </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>


<!-- Page JS Code -->
<script>
    $('.hapus').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      swal({
          title: "Yakin Ingin Hapus?",
          text: "Data yang sudah dihapus tidak dapat dikembalikan!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn-danger",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            swal("Berhasil!", "Data berhasil dihapus!", "success");
            window.location.replace(url);
          } else {
            swal("Batal!", "Data tidak jadi terhapus!", "error");
          }
        });
    });

    //select2
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

    //JQuery for Update Data
    $(document).ready(function() {
          $('#edit-data').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)

              modal.find('#val-id-kasir').attr("value",div.data('id-kasir'));
              modal.find('#val-nama-kasir').attr("value",div.data('nama-kasir'));
              modal.find('#val-alamat').attr("value",div.data('alamat'));
              modal.find('#val-telepon').attr("value",div.data('telepon'));
              modal.find('#val-status').attr("value",div.data('status'));
              modal.find('#val-username').attr("value",div.data('username'));
              modal.find('#val-akses').attr("value",div.data('akses'));
          });
      });
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>