<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Content -->
<div class="content content-narrow hidden-print">

<div class="row">
    <div class="col-md-6">
        <form class="js-form-search push-10 form-inline" action="<?php echo base_url()?>admin/laporan/stok" method="post">
<!--             <div class="form-group">
                <label class="control-label">Cari Laporan : </label>
                <?php
                echo "
                <select class='js-select2 form-control' name='kat' id='val-kategori' style='width: 100%;' data-placeholder='Pilih Kategori'>
                 <option value='' disabled selected>aa</option>";
                  foreach ($kategori as $k) {  
                  echo "<option value='".$k['kategori']."'>".$k['kategori']."</option>";
                  }
                  echo"
                </select>";
                ?>
            </div> -->
            <div class="form-group">
            <?php
            echo
            "<select class='js-select2 form-control' id='kat' name='kat' style='width: 250px;' data-placeholder='Pilih Kategori'>
                ";

                foreach ($kategori as $k) {  
                echo "<option value='".$k['kategori']."'>".$k['kategori']."</option>";
                }
            echo "</select>";

            ?>
            </div>
        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>
</div>
<div class="content content-narrow">
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block">
                        <div class="block-header">
                            <ul class="block-options">
                                <li>
                                    <button type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"> Cetak Laporan</i>  </button>
                                </li>
                            </ul>
                        </div>
                            <div class="block-content block-content-narrow">
                            <div class="h1 text-center push-30-t push-30">LAPORAN STOK BUKU</div>
                            <hr>
                            <div class="row items-push-2x">
                                <div class="col-xs-6 col-sm-2 col-lg-10">
                                    <address>
                                        Perusahaan : OneBook Book Store <br>
                                        Alamat : Jalan Letnan Arsyad No. 15 Kayuringin Jaya, Bekasi Selatan
                                    </address>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="table-responsive push-50">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">Kategori</th>
                                        <th class="text-center">Judul</th>
                                        <th class="text-center">Stok</th>
                                        <th class="text-center">Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach($data as $k) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++?></td>
                                        <td class="text-center" width="50%"><?php echo $k->kategori ?></td>
                                        <td class="text-center" width="50%"><?php echo $k->judul ?></td>
                                        <td class="text-center" width="50%"><?php echo $k->stok ?></td>
                                        <td class="text-center" width="50%"><?php echo "Rp. ".number_format($k->harga_jual) ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<script src="<?php echo base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script>
    //select2 
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });
    //daterpicker
    $("#val-tahun").datepicker( {
        format: " yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>