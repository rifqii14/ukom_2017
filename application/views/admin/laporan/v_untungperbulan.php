<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Content -->
<div class="content content-narrow hidden-print">

<div class="row">
    <div class="col-md-6">
        <form class="js-form-search push-10 form-inline" action="<?php echo base_url()?>admin/laporan/untungperbulan" method="post">
            <div class="form-group">
                <label class="control-label">Cari Laporan : </label>
                <input class="form-control" type="text" id="val-tahun" name="tahun" placeholder="Pilih Tahun">
            </div>
        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>
</div>
<div class="content content-narrow">
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block">
                        <div class="block-header">
                            <ul class="block-options">
                                <li>
                                    <button type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"> Cetak Laporan</i>  </button>
                                </li>
                            </ul>
                        </div>
                            <div class="block-content block-content-narrow">
                            <div class="h1 text-center push-30-t push-30">LAPORAN KEUNTUNGAN PER BULAN - TAHUN <?php echo $tahun?></div>
                            <hr>
                            <div class="row items-push-2x">
                                <div class="col-xs-6 col-sm-2 col-lg-10">
                                    <address>
                                        Perusahaan : OneBook Book Store <br>
                                        Alamat : Jalan Letnan Arsyad No. 15 Kayuringin Jaya, Bekasi Selatan
                                    </address>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="table-responsive push-50">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="7%">No.</th>
                                        <th class="text-center" width="7%">Total Keuntungan</th>
                                        <th class="text-center" width="7%">Bulan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach($data as $k) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++?></td>
                                        <td class="text-center" width="50%"><?php echo "Rp. ".number_format($k->keuntungan) ?></td>
                                        <td class="text-center" width="50%"><?php echo $k->bulan ?>, <?php echo $k->tahun ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<script src="<?php echo base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js')?>""></script>
<script>
    //daterpicker
    $("#val-tahun").datepicker( {
        format: " yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>