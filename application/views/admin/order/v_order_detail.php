 <?php 
 $this->load->view('layout/template_head_start');
 ?>
 <script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
 <style type="text/css">
 @page {
     size: A4;
     margin: 0;
 }
 </style>
 <?php
 $this->load->view('layout/template_head_end.php');
 $this->load->view('layout/base_head.php');
 ?>

 <!-- Page Content -->
 <div class="content content-boxed" style="margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;">

     <div class="block">
         <div class="block-content block-content-narrow">
             
             <div class="h1 text-center push-30-t push-30">DETAIL PENJUALAN</div>
             <hr>
             <div class="row items-push-2x">
                 <div class="col-xs-6 col-sm-2 col-lg-10">
                     <address>
                         Nomor Referensi : <?php echo $order['no_referensi']?> <br>
                         Nama Kasir : <?php echo $order['nama']?><br>
                         Tanggal : <?php echo $this->general->humanDate($order['tanggal'])?><br>
                     </address>
                 </div>
             </div>

             <!-- Table -->
             <div class="table-responsive push-50">
                 <table class="table table-bordered">
                     <thead>
                         <tr>
                             <th class="text-center" style="width: 50px;"></th>
                             <th>Judul Buku</th>
                             <th class="text-center" style="width: 100px;">Jumlah</th>
                             <th class="text-right" style="width: 120px;">Harga</th>
                             <th class="text-right" style="width: 120px;">Subtotal</th>
                         </tr>
                     </thead>
                     <tbody>
                     <?php $no = 1; foreach ($orderDetails as $orderDetail){ ?>
                         <tr>
                             <td class="text-center"><?php echo $no++?></td>
                             <td>
                                 <p class="font-w600 push-10"><?php echo $orderDetail['judul']; ?></p>
                             </td>
                             <td class="text-center"><span class="badge badge-primary"><?php echo $orderDetail['jumlah'] ?></span></td>
                             <td class="text-right"><?php echo "Rp. ".number_format($orderDetail['harga_jual']) ?></td>
                             <td class="text-right"><?php echo "Rp. ".number_format($orderDetail['subtotal']) ?></td>
                         </tr>
                    <?php } ?>
                    <tr class="active">
                        <td colspan="4" class="font-w700 text-uppercase text-right">Total : </td>
                        <td class="font-w700 text-right"><?php echo "Rp. ".number_format($order['total']); ?></td>
                    </tr>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
    
 </div>
 <!-- END Page Content -->
 

 <?php 
 $this->load->view('layout/base_footer.php');
 ?>
 <?php
 $this->load->view('layout/template_footer_start.php');
 ?>
 <?php
 $this->load->view('layout/template_footer_end.php');
 ?>
