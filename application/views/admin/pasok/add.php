<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Tambah Pasok
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <!-- Bootstrap Forms Validation -->
            <div class="block">
                <div class="block-content block-content-narrow">
                    <base href="<?php echo base_url(); ?>" />
                    <!-- <?php echo form_open_multipart('admin/Pasok/save',array('class' => 'js-validation-bootstrap form-horizontal'))?> -->

                    <form class="js-validation-bootstrap form-horizontal" id="form_pasok" role="form">

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Distributor<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control reset' name='val-nama-distributor' id='val-nama-distributor' style='width: 100%;' data-placeholder='Pilih Distributor'>
                                 <option value='' disabled selected></option>";
                                  foreach ($distributor as $k) {  
                                  echo "<option value='".$k['id_distributor']."'>".$k['nama_distributor']."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Judul Buku<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control reset' name='val-judul' id='val-judul' style='width: 100%;' data-placeholder='Pilih Judul Buku'>
                                 <option value='' disabled selected></option>";
                                  foreach ($buku as $k) {  
                                  echo "<option value='".$k['id_buku']."'>".$k['judul']."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-jumlah">Jumlah Pasok<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control reset" type="text" id="val-jumlah" name="val-jumlah" placeholder="Masukkan Jumlah Pasok">
                            </div>
                        </div>

                        <div class="form-group">
<!--                             <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                            </div> -->
                            <div class="col-md-8 col-md-offset-2">
                            <button id = "tambah" class="btn btn-block btn-primary"><i class="fa fa-cart-plus"> Tambahkan</i></button>
                            </div>
                        </div>
                    <!-- <?php echo form_close(); ?> -->
                    </form>

                </div>
                <div class="block">
                    <div class="block-content block-content-narrow">
                <table id="table_pasok" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">id</th>
                            <th class="text-center">distributor</th>
                            <th class="text-center">id</th>
                            <th class="text-center">judul buku</th>
                            <th class="text-center">jumlah pasok</th>
                            <th class="text-center">aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="form-group">
                    <button id="selesai" class="btn btn-lg btn-block btn-primary selesai">
                        <i class="fa fa-check"> Selesai</i>
                    </button>
                </div>
                </div>
                </div>
            </div>
            <!-- Bootstrap Forms Validation -->
        </div>
    </div>
    <!-- END Forms Row -->

</div>
<!-- END Page Content -->
<script src="<?php echo base_url('assets/js/plugins/jscoba/dynamictable.js')?>"></script>

<script>
    //select2 
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

    var dTable = $('table').DynamicTable({
        tabData: {aaaaaaa:"",id_distributor:"",distributor:"",id_buku:"",judul:"",jumlah: ""},
        delButtonClass: "btn btn-block btn-danger",
        delButtonText: "<i class='fa fa-times'></i>",
        dataIdName: "aaaaaaa",
        hasFirstRow: true
    });

    dTable.setDelFunc(function(ob) {
    });

    $('#tambah').on('click',function(e) {
        e.preventDefault();
        if ($('#form_pasok').valid()){
            dTable.add({
                "distributor" : $('#val-nama-distributor option:selected').html(),
                "id_distributor": $('#val-nama-distributor').val(),
                // "nama_distributor" : $('#val-judul').val(),
                "id_buku" : $('#val-judul').val(),
                "judul": $('#val-judul option:selected').html(),
                // "judul" : $('#harga-jual').val(),
                "jumlah": $('#val-jumlah').val()
            }, function(e) {
                $('.reset').val(''); 
            });
        }
    });

    $('#selesai').on('click',function(e) {
        e.preventDefault();

        $.post("<?php echo base_url('admin/Pasok/save') ?>", {kirim: JSON.stringify(dTable.getData())}, function(res) {
            alert('Berhasil!');
            window.location.href="<?php echo base_url('admin/Pasok/')?>";

        }, "json")
    });

    // $('#selesai').on('click',function(e) {
    //  e.preventDefault();
    //  console.log(dTable.getData());
    //  // dTable.clear();
    // });

</script>

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>

<!-- Page JS Code -->

<?php
$this->load->view('layout/template_footer_end.php');
?>