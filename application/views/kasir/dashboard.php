<?php 
$this->load->view('layout/template_head_start');
?>
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugins/chartjsv2/Chart.js')?>"></script>
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Dashboard Kasir
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Stats -->
<div class="row text-uppercase">
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="font-s12 font-w700"> <i class="fa fa-book"></i>  Jumlah Buku</div>
                <a class="h2 font-w300 text-primary"><?php echo $jumlah_buku?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="font-s12 font-w700"> <i class="fa fa-truck"></i> Jumlah Distributor</div>
                <a class="h2 font-w300 text-primary"><?php echo $jumlah_distributor?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                    <small><i class="si si-calendar"></i> <?php echo $untung['bulan'];?></small>
                </div>
                <div class="font-s12 font-w700">Keuntungan Bulan Ini</div>
                <a class="h2 font-w300 text-primary"><?php echo "Rp. ".number_format($untung['keuntungan']);?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                   <small><i class="si si-calendar"></i> <?php echo $this->general->humanDate2(date('l, d-m-Y')) ?></small> 
                </div>
                <div class="font-s12 font-w700">Keuntungan Hari Ini</div>
                <a class="h2 font-w300 text-primary"><?php echo "Rp. ".number_format($untung_hari['keuntungan']);?></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


$(document).ready(function(){
var data = {
        labels: <?php echo $label; ?>,
        datasets: [
            {
                label: "Total Pasokan Buku Per Bulan, Tahun: " + <?php echo $one;?>,
                fill:  true,
                backgroundColor: 'rgba(125, 179, 227, .3)',
                borderColor: 'rgba(125, 179, 227, 1)',
                pointBackgroundColor: 'rgba(125, 179, 227, 1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(125, 179, 227, 1)',
                data: <?php echo $result1; ?>
            }
        ]
    };

    var ctx = document.getElementById("demoChart").getContext("2d");
    var chart = new Chart(ctx, { type: 'line', data: data,
    options: {
      scaleOverride: true,
      scaleSteps: 10,
      scaleStartValue: -10,
      scales: {
        yAxes: [{
          position: "left",
          scaleLabel: {
            display: true,
            labelString: "Buku",
            fontFamily: "Open Sans",
            fontColor: "black",
          },
          ticks: {
            fontFamily: "Open Sans"
          },
        }],
      },
    } });

    // document.getElementById("demoLegend").innerHTML = chart.generateLegend();
});
</script>

<!-- BATAS -->

<script type="text/javascript">
$(document).ready(function(){
    var data = {
        labels: <?php echo $label2; ?>,
        datasets: [
            {
                label: "Total Keuntungan Per Bulan, Tahun: " + <?php echo $one2;?>,
                fill:  true,
                backgroundColor: 'rgba(125, 179, 227, .3)',
                borderColor: 'rgba(125, 179, 227, 1)',
                pointBackgroundColor: 'rgba(125, 179, 227, 1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(125, 179, 227, 1)',
                data: <?php echo $result2; ?>
            }
        ]
    };

    var ctx = document.getElementById("demoChart2").getContext("2d");
    var chart = new Chart(ctx, { type: 'bar', data: data,
    options: {
      scaleOverride: true,
      scaleSteps: 10,
      scaleStartValue: -10,
      scales: {
        yAxes: [{
          position: "left",
          scaleLabel: {
            display: true,
            labelString: "Rupiah",
            fontFamily: "Open Sans",
            fontColor: "black",
          },
          ticks: {
            fontFamily: "Open Sans"
          },
        }],
      },
    } });

    // document.getElementById("demoLegend").innerHTML = chart.generateLegend();
});
</script>
<!-- Dashboard Charts -->
<div class="row">
    <div class="col-md-6">
        <div class="block block-rounded block-opt-refresh-icon8">
            <div class="block-header">
                <h3 class="block-title">Total Pasokan Per Bulan Tahun Ini</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">
                <div style="height: 340px;"><canvas id="demoChart"></canvas></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="block block-rounded block-opt-refresh-icon8">
            <div class="block-header">
                <h3 class="block-title">Total Keuntungan Per Bulan Tahun Ini</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">
                <div style="height: 340px;"><canvas id="demoChart2"></canvas></div>
            </div>
        </div>
    </div>
</div>
<!-- END Dashboard Charts -->
</div>
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
$this->load->view('layout/template_footer_start.php');
$this->load->view('layout/template_footer_end.php');
?>