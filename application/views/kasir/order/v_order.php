<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Data Penjualan
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" width="7%">No.</th>
                    <th>No. Referensi</th>
                    <th>Kasir</th>
                    <th>Tanggal Order</th>
                    <th>Total</th>
                    <th class="text-center" width="7%">Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php $no = 1; foreach($data as $o) { ?>
                <tr>
                    <td class="text-center"><?php echo $no++?></td>
                    <td><?php echo $o['no_referensi']?></td>
                    <td class="font-w600"><?php echo $o['nama'] ?></td>
                    <td><?php echo $this->general->humanDate($o['tanggal']); ?></td>
                    <td><?php echo "Rp. ".number_format($o['total']) ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="<?= base_url().'kasir/order/detail/'.$o['id_penjualan']; ?>" class="hapus">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Detail"><i class="icon-eye4 "></i></button>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
<!-- END Dynamic Table Full -->
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>

<!-- Page JS Code -->
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<?php
$this->load->view('layout/template_footer_end.php');
?>