<?php 
$this->load->view('layout/template_head_start');
?>

<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<style type="text/css">
	input[readonly]:hover
	{
	    cursor:not-allowed;
	}
</style>

<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>


<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
               Tambah Penjualan
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<div class="row">
<form class="js-validation-bootstrap form-horizontal" id="form_transaksi" role="form">
	<div class="col-lg-6">
	    <div class="block">
	        <div class="block-content block-content-narrow">
	            <div class="form-group">
	                <div class="col-md-4">
	                    <label class="control-label" for="val-isbn">No. ISBN : </label>
	                </div>
	                <div class="col-md-8">
	                    <div class="input-group">
	                        <input autocomplete ="off" class="form-control reset" type="text" id="val-isbn" name="val-isbn" placeholder="Cari berdasarkan ISBN">
	                        <div class="input-group-btn">
	                               <a class="btn btn-default" id="btncari">
	                               	Cari</a>
	                         </div>
	                    </div>
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-4">
	                <label class="control-label" for="val-judul">Judul : </label>
	                </div>
	                <div class="col-md-8">
	                	<input type="hidden" id="val-id-buku" name="val-id-buku">
	                    <input class="form-control reset" type="text" id="val-judul" name="val-judul" readonly>
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-4">
	                <label class="control-label" for="harga-jual">Harga : </label>
	                </div>
	                <div class="col-md-8">
	                    <div class="input-group">
	                        <span class="input-group-addon">Rp.</span>
	                        <input class="form-control reset" type="text" id="harga-jual" name="harga-jual" readonly>
	                    </div>
	                    <small><span class="text-danger"><i>*) sudah termasuk diskon dan ppn 10%</i></span></small>
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-4">
	                <label class="control-label" for="qty">Quantity : </label>
	                </div>
	                <div class="col-md-3">
	                    <div class="input-group">
	                        <input class="form-control reset" type="number" id="qty" name="qty"><br>
	                    </div>
	                </div>
	                <h2 style="font-size: 12px" class ="label label-success">Stok : <span class="reset" id="val-stok"></span></h2>
	            </div>
	            <div class="form-group">
	                <div class="col-md-4">
	                <label class="control-label" for="subtotal">Subtotal : </label>
	                </div>
	                <div class="col-md-8">
	                    <div class="input-group">
	                        <span class="input-group-addon">Rp.</span>
	                        <input class="form-control reset" type="text" id="subtotal" name="subtotal" readonly>
	                    </div>
	                </div>
	            </div>
	            <div class="form-group">
		            <div class="col-md-4">
		            	<label> </label>
		            </div>	
	                <div class="col-md-8">
	                    <button id = "tambah" class="btn btn-block btn-primary"><i class="fa fa-cart-plus"> Tambahkan</i></button>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="col-lg-6">
	    <div class="block">
	        <div class="block-content block-content-narrow">
	        	<div class="form-group">
	        	<div class="col-md-3">
	        		<label for="total" class="besar">Total :</label>
	        	</div>
	        	<div class="col-md-3">
	        			<h2 style="font-size: 15px" class ="label label-success">Rp. <span id="total"></span></h2>
	        	</div>
	        	</div>
	        	<div class="form-group">
	        	<div class="col-md-3">
	        		<label for="bayar">Bayar :</label>
	        	</div>
	        	<div class="col-md-8">
	        		<div class="input-group">
		        		<span class="input-group-addon">Rp.</span>
		        		<input type="number" min="0" class="form-control" id="bayar" name="bayar">
	        		</div>
	        	</div>
	        	</div>
	        	<div class="form-group">
	        	<div class="col-md-3">
	        		<label for="kembali">Kembali :</label>
	        	</div>
	        	<div class="col-md-3">
						<h2 style="font-size: 15px" class ="label label-success">Rp. <span id="kembali"></span></h2>
				</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
</form>
<div class="block">
    <div class="block-content block-content-narrow">
<table id="table_transaksi" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th class="text-center">judul</th>
			<th class="text-center">harga</th>
			<th class="text-center">quantity</th>
			<th class="text-center">subtotal</th>
			<th class="text-center">id</th>
			<th class="text-center">aksi</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div class="form-group">
    <button id="selesai" class="btn btn-lg btn-block btn-primary selesai">
    	<i class="fa fa-check"> Selesai</i>
    </button>
</div>
</div>
</div>

</div>
<!-- END Forms Row -->
</div>
<!-- END Page Content -->
<script src="<?php echo base_url('assets/js/plugins/jscoba/dynamictable.js')?>"></script>
<script type="text/javascript">
	// show data buku
	$('#btncari').on('click',function(e) {
		e.preventDefault();
		var datas = {
			noisbn:$('#val-isbn').val()
		}
		$.post( "<?php echo base_url('kasir/penjualan/getBuku')?>", datas, function( data ) {
		  $( ".result" ).html( data );
		  var hasil = $.parseJSON(data);
		  $('#val-id-buku').val(hasil.id_buku);
		  $('#val-judul').val(hasil.judul);
		  $('#val-stok').text(hasil.stok);
		  // $('#val-diskon').val(parseInt(((1-hasil.diskon))*100));
		  $('#harga-jual').val(hasil.harga_jual);
		  if(hasil.stok == 0){
		  	$('#qty').val(0).attr( {"readonly":true, "min":1, "max": hasil.stok} );
		  	$('#subtotal').val(0);
		  }
		  else{
		  	$('#qty').attr( {"readonly":false, "min":1, "max": hasil.stok} ).on('click keyup', function() {
		  			$("#subtotal").val($('#harga-jual').val() * $('#qty').val());
		  		});
		  }
		});

		$('#qty').focus();
		$('#qty').val('');

	});


	//hitung kembali
	$('#bayar').on('click keyup paste', function() {
			$("#kembali").text($('#bayar').val() - $('#total').text());
		});

	//table untuk menampung data jual
	var total = 0;
	var dTable = $('table').DynamicTable({
		tabData: {aaaaaaa:"",judul:"", harga_jual:"", jumlah: "", subtotal: ""},
		delButtonClass: "btn btn-block btn-danger",
		delButtonText: "<i class='fa fa-times'></i>",
		dataIdName: "aaaaaaa",
		hasFirstRow: true
	});

	dTable.setDelFunc(function(ob) {
		total -= parseInt(ob.subtotal);
		$("#total").text(total);
	});

	$('#tambah').on('click',function(e) {
		e.preventDefault();
		if ($('#form_transaksi').valid()){
			dTable.add({
				"id_buku": $('#val-id-buku').val(),
				"judul" : $('#val-judul').val(),
				"harga_jual" : $('#harga-jual').val(),
				"jumlah": $('#qty').val(),
				"subtotal": $('#subtotal').val()
			}, function(e) {
				$('.reset').val(''); 
				$('.reset').text('');  
				total += parseInt(e.subtotal);
				$("#total").text(total);
			});
			$('#val-isbn').focus();
		}
	});

	$('#selesai').on('click',function(e) {
		e.preventDefault();

		$.post("<?php echo base_url('kasir/penjualan/simpan') ?>", {kirim: JSON.stringify(dTable.getData())}, function(res) {
			alert('Berhasil!');
			window.location.href="<?php echo base_url('kasir/order/')?>";

		}, "json")
	});

	// $('#selesai').on('click',function(e) {
	// 	e.preventDefault();
	// 	console.log(dTable.getData());
	// 	// dTable.clear();
	// });

</script>

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<script src="<?php echo base_url('assets/js/plugins/tablejson/tablejson.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>

<?php
$this->load->view('layout/template_footer_end.php');
?>