<?php  

if ($this->session->userdata('akses') == 'Admin'){
    $c = "";
    $d = "hidden";
    $e = "hidden";
}
else if ($this->session->userdata('akses') == 'Kasir'){
    $c = "hidden";
    $d = "" ;
    $e = "hidden";
}
else{
    $c = "hidden";
    $d = "hidden" ;
    $e = "";
}

?>

            <!-- Sidebar -->
            <nav id="sidebar">

                <div id="sidebar-scroll">
                    
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                        <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times"></i>
                        </button>
                            <a class="h5 text-white">
                                <span class="h4 font-w600 sidebar-mini-hide"><img width="170px" height="25%" src=<?php echo base_url("assets/img/oneBook.png") ?>></span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li class = "<?php echo $c; ?>">
                                    <a class=" <?php echo $c; ?><?=(current_url()==base_url('admin/Dashboard')) ? 'active':''?>" href="<?php echo site_url('admin/Dashboard') ?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>

                                <!-- kasir -->
<!--                                 <li class="<?php echo $d ?>">
                                    <a class="<?=(current_url()==base_url('kasir/Dashboard')) ? 'active':''?>" href="<?php echo site_url('kasir/Dashboard') ?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li> -->
                                <!-- end kasir -->

                                <!-- petugas -->
                                <li class="<?php echo $e ?>">
                                    <a class="<?=(current_url()==base_url('petugas/Dashboard')) ? 'active':''?>" href="<?php echo site_url('petugas/Dashboard') ?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>
                                <!-- end petugas -->

                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Main Navigation</span></li>
                                <!-- kasir -->
                                <li class = "<?php echo $d;?>">
                                    <a class="<?=(current_url()==base_url('kasir/Penjualan')) ? 'active':''?>" href="<?php echo site_url('kasir/Penjualan') ?>"><i class="fa fa-cart-plus"></i><span class="sidebar-mini-hide">Tambah Penjualan</span></a>
                                </li>
                                <li class = "<?php echo $d; ?>">
                                    <a class="<?=(current_url()==base_url('kasir/Order')) ? 'active':''?>" href="<?php echo site_url('kasir/Order') ?>"><i class="icon-cart"></i><span class="sidebar-mini-hide">Data Penjualan</span></a>
                                </li>
                                <!-- end kasir -->

                                <li class="<?php $base = $this->uri->segment(1) == 'admin';
                                                if( ($base && $this->uri->segment(2) == 'Buku') || ($base && $this->uri->segment(2) == 'Kategori') || ($base && $this->uri->segment(2) == 'Pasok') || ($base && $this->uri->segment(2) == 'Diskon')) 
                                                {
                                                    echo 'open';
                                                }
                                                 else 
                                                {
                                                    echo '';
                                                } 
                                                echo $c;?>">
                                <a class="nav-submenu" data-toggle="nav-submenu"><i class="si si-layers"></i><span class="sidebar-mini-hide">Data Produk</span></a>
                                <ul>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/Buku')) ? 'active':''?>"  href="<?php echo site_url('admin/Buku') ?>"><i class="fa fa-book"></i>Data Buku</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/Kategori')) ? 'active':''?>"  href="<?php echo site_url('admin/Kategori') ?>"><i class="si si-list"></i>Data Kategori</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/Pasok')) ? 'active':''?>"  href="<?php echo site_url('admin/Pasok') ?>"><i class="icon-dropbox"></i>Data Pasok</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/Diskon')) ? 'active':''?>"  href="<?php echo site_url('admin/Diskon') ?>"><i class="icon-percent"></i>Data Diskon</a>
                                    </li>
                                </ul>
                                </li>

                                <!-- petugas -->

                                <li class="<?php $base = $this->uri->segment(1) == 'petugas';
                                                if( ($base && $this->uri->segment(2) == 'Buku') || ($base && $this->uri->segment(2) == 'Kategori') || ($base && $this->uri->segment(2) == 'Pasok') || ($base && $this->uri->segment(2) == 'Diskon')) 
                                                {
                                                    echo 'open';
                                                }
                                                 else 
                                                {
                                                    echo '';
                                                } 
                                                echo $e;?>">
                                <a class="nav-submenu" data-toggle="nav-submenu"><i class="si si-layers"></i><span class="sidebar-mini-hide">Data Produk</span></a>
                                <ul>
                                    <li>
                                        <a class="<?=(current_url()==base_url('petugas/Buku')) ? 'active':''?>"  href="<?php echo site_url('petugas/Buku') ?>"><i class="fa fa-book"></i>Data Buku</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('petugas/Kategori')) ? 'active':''?>"  href="<?php echo site_url('petugas/Kategori') ?>"><i class="si si-list"></i>Data Kategori</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('petugas/Pasok')) ? 'active':''?>"  href="<?php echo site_url('petugas/Pasok') ?>"><i class="icon-dropbox"></i>Data Pasok</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('petugas/Diskon')) ? 'active':''?>"  href="<?php echo site_url('petugas/Diskon') ?>"><i class="icon-percent"></i>Data Diskon</a>
                                    </li>
                                </ul>
                                </li>
                                <!-- end petugas -->

                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Distributor')) ? 'active':''?>" href="<?php echo site_url('admin/Distributor') ?>"><i class="fa fa-truck"></i><span class="sidebar-mini-hide">Data Distributor</span></a>
                                </li>
                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Pegawai')) ? 'active':''?>" href="<?php echo site_url('admin/Pegawai') ?>"><i class="fa fa-user"></i><span class="sidebar-mini-hide">Data Pegawai</span></a>
                                </li>
                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Order')) ? 'active':''?>" href="<?php echo site_url('admin/Order') ?>"><i class="icon-cart"></i><span class="sidebar-mini-hide">Data Penjualan</span></a>
                                </li>
                                <li class="<?php $base = $this->uri->segment(1) == 'admin';
                                                if( ($base && $this->uri->segment(2) == 'laporan' && $this->uri->segment(3) == 'untungperhari') || ($base && $this->uri->segment(2) == 'laporan' && $this->uri->segment(3) == 'untungperbulan') || ($base && $this->uri->segment(2) == 'laporan' && $this->uri->segment(3) == 'pengeluaranperbulan') || ($base && $this->uri->segment(2) == 'laporan' && $this->uri->segment(3) == 'stok')) 
                                                {
                                                    echo 'open';
                                                }
                                                 else 
                                                {
                                                    echo '';
                                                } 
                                                echo $c; ?>">
                                <a class="nav-submenu" data-toggle="nav-submenu"><i class="fa fa-newspaper-o"></i><span class="sidebar-mini-hide">Laporan</span></a>
                                <ul>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/laporan/untungperhari')) ? 'active':''?>"  href="<?php echo site_url('admin/laporan/untungperhari') ?>">Keuntungan Per Hari</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/laporan/untungperbulan')) ? 'active':''?>"  href="<?php echo site_url('admin/laporan/untungperbulan') ?>">Keuntungan Per Bulan</a>
                                    </li>
                                    <li>
                                        <a class="<?=(current_url()==base_url('admin/laporan/stok')) ? 'active':''?>"  href="<?php echo site_url('admin/laporan/stok') ?>">Stok Buku</a>
                                    </li>
                                </ul>
                                </li>

                                <li class = "<?php echo $c;?>">
                                    <a class="<?=(current_url()==base_url('admin/Backup')) ? 'active':''?>" href="<?php echo site_url('admin/Backup') ?>"><i class="icon-file-download2"></i><span class="sidebar-mini-hide">Backup Database</span></a>
                                </li>


                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Company</span></li>
                                <li>
                                    <a href="<?php echo site_url('About') ?>" target="_blank"><i class="si si-info"></i><span class="sidebar-mini-hide">Tentang Kami</span></a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('Help') ?>" target="_blank"><i class="si si-question"></i><span class="sidebar-mini-hide">Bantuan</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->