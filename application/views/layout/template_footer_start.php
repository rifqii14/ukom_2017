<!-- JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.scrollLock.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.appear.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.countTo.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.placeholder.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/core/js.cookie.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/app.js') ?>"></script>