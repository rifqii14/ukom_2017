<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Tambah Buku
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <!-- Bootstrap Forms Validation -->
            <div class="block">
                <div class="block-content block-content-narrow">
                    <base href="<?php echo base_url(); ?>" />
                    <?php echo form_open_multipart('petugas/Buku/save',array('class' => 'js-validation-bootstrap form-horizontal'))?>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Kategori <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control' name='val-kategori' id='val-kategori' style='width: 100%;' data-placeholder='Pilih Kategori'>
                                 <option value='' disabled selected></option>";
                                  foreach ($kategori as $k) {  
                                  echo "<option value='".$k['id_kategori']."'>".$k['kategori']."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-judul">Judul <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-judul" name="val-judul" placeholder="Masukkan Judul Buku">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="filefoto">Cover <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="file" id="filefoto" name="filefoto">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-noisbn">No. ISBN <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-noisbn" name="val-noisbn" placeholder="Masukkan Nomor ISBN">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-penulis">Penulis <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-penulis" name="val-penulis" placeholder="Masukkan Nama Penulis">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-penerbit">Penerbit <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-penerbit" name="val-penerbit" placeholder="Masukkan Nama Penerbit">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-tahun">Tahun Terbit <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-tahun" name="val-tahun" placeholder="Masukkan Tahun Terbit">
                            </div>
                        </div>

<!--                         <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-stok">Stok </label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-stok" name="val-stok" placeholder="Masukkan Stok">
                            </div>
                        </div>
 -->
                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-pokok">Harga Pokok <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input class="form-control" type="text" id="val-pokok" name="val-pokok" placeholder="Masukkkan Harga Pokok">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-ppn">PPN <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                <input value="10" class="form-control" type="text" id="val-ppn" name="val-ppn" placeholder="Masukkan PPN" readonly>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Diskon <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control' name='val-diskon' id='val-diskon' style='width: 100%;' data-placeholder='Pilih Diskon'>
                                 <option value='' disabled selected></option>";
                                  foreach ($diskon as $d) {  
                                  echo "<option value='".$d['diskon']."'>".(1-$d['diskon'])*(100).'%'."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- Bootstrap Forms Validation -->
        </div>
    </div>
    <!-- END Forms Row -->

</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>

<!-- Page JS Code -->
<script>
    //select2 
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

    //daterpicker
    $("#val-tahun").datepicker( {
        format: " yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
</script>

<?php
$this->load->view('layout/template_footer_end.php');
?>