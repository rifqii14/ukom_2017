<?php 
$this->load->view('layout/template_head_start');
?>
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugins/chartjsv2/Chart.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/filesaver/FileSaver.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/canvas-blob/canvas-toBlob.js')?>"></script>
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Dashboard Petugas
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Stats -->
<div class="row text-uppercase">
<div class="col-xs-6 col-sm-6">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="font-s12 font-w700"> <i class="fa fa-book"></i>  Jumlah Buku</div>
                <a class="h2 font-w300 text-primary"><?php echo $jumlah_buku?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="font-s12 font-w700"> <i class="fa fa-truck"></i> Jumlah Distributor</div>
                <a class="h2 font-w300 text-primary"><?php echo $jumlah_distributor?></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


$(document).ready(function(){
var data = {
        labels: <?php echo $label; ?>,
        datasets: [
            {
                label: "Total Pasokan Buku Per Bulan, Tahun: " + <?php echo $one;?>,
                fill:  false,
                backgroundColor: 'rgba(125, 179, 227, .3)',
                borderColor: 'rgba(125, 179, 227, 1)',
                pointBackgroundColor: 'rgba(125, 179, 227, 1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(125, 179, 227, 1)',
                data: <?php echo $result1; ?>
            }
        ]
    };

    var ctx = document.getElementById("statistik").getContext("2d");
    var chart = new Chart(ctx, { type: 'line', data: data,
    options: {
      scaleOverride: true,
      scaleSteps: 10,
      scaleStartValue: -10,
      scales: {
        yAxes: [{
          position: "left",
          scaleLabel: {
            display: true,
            labelString: "Buku",
            fontFamily: "Open Sans",
            fontColor: "black",
          },
          ticks: {
            fontFamily: "Open Sans"
          },
        }],
      },
    } });

    // document.getElementById("demoLegend").innerHTML = chart.generateLegend();
});
</script>

<!-- Dashboard Charts -->
<div class="row">
    <div class="col-sm-12">
        <div class="block block-rounded block-opt-refresh-icon8">
            <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" id="png1"><i class="fa fa-download"> PNG</i>  </button>
                </li>
            </ul>
                <h3 class="block-title">Statistik Total Pasokan Per Bulan Tahun Ini</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">
                <div style="height: 5%;width: 100%;"><canvas id="statistik"></canvas></div>
            </div>
        </div>
    </div>
</div>
<!-- END Dashboard Charts -->
</div>
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<script type="text/javascript">
    $("#png1").click(function() {
            $("#statistik").get(0).toBlob(function(blob) {
                saveAs(blob, "Total Pasok Per Bulan Tahun <?php echo date('Y');?>.PNG");
            });
    });
</script>
<?php
$this->load->view('layout/template_footer_start.php');
$this->load->view('layout/template_footer_end.php');
?>