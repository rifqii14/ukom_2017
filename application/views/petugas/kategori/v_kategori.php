<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Data Kategori
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
        <p class="text-muted font-13 m-b-30">
          <a href="<?php echo site_url('petugas/Kategori/form') ?>">
          <button style="width:100px;" class="btn btn-success btn-block"><span class="icon-plus3"></span> Tambah</button>
          </a>
        </p>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" width="7%">No.</th>
                    <th>Kategori</th>
                    <th class="text-center" width="7%">Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php $no = 1; foreach($data as $k) { ?>
                <tr>
                    <td class="text-center"><?php echo $no++?></td>
                    <td class="font-w600" width="50%"><?php echo $k['kategori'] ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a 
                            href="javascript:;"
                            data-id-kategori="<?php echo $k['id_kategori'] ?>"
                            data-kategori="<?php echo $k['kategori'] ?>"
                            data-toggle="modal" data-target="#edit-data">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#ubah-data" title="Edit Kategori"><i class="fa fa-pencil"></i></button>
                            </a>
                            <a href="<?= base_url().'petugas/Kategori/remove/'.$k['id_kategori']; ?>" class="hapus">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Hapus Kategori"><i class="fa fa-times"></i></button>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
<!-- END Dynamic Table Full -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Ubah Kategori</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" action="<?php echo base_url('petugas/Kategori/modified')?>"  method="post" enctype="multipart/form-data" role="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Kategori</label>
                         <div class="col-lg-10">
                          <input type="hidden" id="val-id-kategori" name="val-id-kategori">
                             <input type="text" class="form-control" id="val-kategori" name="val-kategori" placeholder="Masukkan nama kategori">
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Data -->
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>

<!-- Page JS Code -->
<script>
    $('.hapus').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      swal({
          title: "Yakin Ingin Hapus?",
          text: "Data yang sudah dihapus tidak dapat dikembalikan!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn-danger",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            swal("Berhasil!", "Data berhasil dihapus!", "success");
            window.location.replace(url);
          } else {
            swal("Batal!", "Data tidak jadi terhapus!", "error");
          }
        });
    });

    //JQuery for Update Data
    $(document).ready(function() {
          $('#edit-data').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)

              modal.find('#val-id-kategori').attr("value",div.data('id-kategori'));
              modal.find('#val-kategori').attr("value",div.data('kategori'));
          });
      });
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>