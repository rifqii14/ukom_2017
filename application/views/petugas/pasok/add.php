<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datepicker/css/datepicker.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Tambah Pasok
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <!-- Bootstrap Forms Validation -->
            <div class="block">
                <div class="block-content block-content-narrow">
                    <base href="<?php echo base_url(); ?>" />
                    <?php echo form_open_multipart('petugas/Pasok/save',array('class' => 'js-validation-bootstrap form-horizontal'))?>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Distributor<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control' name='val-nama-distributor' id='val-nama-distributor' style='width: 100%;' data-placeholder='Pilih Distributor'>
                                 <option value='' disabled selected></option>";
                                  foreach ($distributor as $k) {  
                                  echo "<option value='".$k['id_distributor']."'>".$k['nama_distributor']."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-select2">Judul Buku<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <?php
                                echo "
                                <select class='js-select2 form-control' name='val-judul' id='val-judul' style='width: 100%;' data-placeholder='Pilih Judul Buku'>
                                 <option value='' disabled selected></option>";
                                  foreach ($buku as $k) {  
                                  echo "<option value='".$k['id_buku']."'>".$k['judul']."</option>";
                                  }
                                  echo"
                                </select>";
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label class="control-label" for="val-jumlah">Jumlah Pasok<span class="text-danger">*</span></label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="val-jumlah" name="val-jumlah" placeholder="Masukkan Jumlah Pasok">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- Bootstrap Forms Validation -->
        </div>
    </div>
    <!-- END Forms Row -->

</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/pages/base_forms_validation.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>

<!-- Page JS Code -->
<script>
    //select2 
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

    //daterpicker
    $("#val-tahun").datepicker( {
        format: " yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
</script>

<?php
$this->load->view('layout/template_footer_end.php');
?>