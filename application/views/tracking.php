<?php 
$this->load->view('layout/template_head_start');
?>

<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">

<style type="text/css">
	input[readonly]:hover
	{
	    cursor:not-allowed;
	}
</style>

<?php
$this->load->view('layout/template_head_end.php');
?>


<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-12">
        <center>
            <h1 class="page-heading">
               TRACKING BUKU YANG INGIN ANDA CARI
            </h1>
        </center>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content content-boxed">
<div class="row">
    <div class="col-md-6">
    <div class="block">
        <div class="block-content block-content-full block-content-narrow">
            <!-- Introduction -->
            <div id="faq1" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse">Tracking Buku Berdasarkan Judul</a>
                        </h3>
                    </div>
                    <div id="faq1_q1" class="panel-collapse collapse in">
                        <div class="panel-body">
                        <form class="js-validation-bootstrap form-horizontal" id="form_transaksi" role="form">
                        	<div class="col-lg-12">
                        	    <div class="block">
                        	        <div class="block-content block-content-narrow">
                        	            <div class="form-group">
                        	                <div class="col-md-4">
                        	                    <label class="control-label" for="val-isbn">Judul: </label>
                        	                </div>
                        	                <div class="col-md-8">
                        	                    <div class="input-group">


                        								<?php
                        								echo
                        								"<select class='js-select2 form-control reset' id='val-isbn' data-placeholder='Pilih Judul Buku'>
                        								   <option value='' disabled selected>Plih Buku</option> ";

                        								    foreach ($book as $k) {  
                        								    echo "<option value='".$k['id_buku']."'>".$k['judul']."</option>";
                        								    }
                        								echo "</select>";

                        								?>

                        	                        <br><br>
                        	                               <a class="btn btn-default" id="btncari">
                        	                               	Cari</a>
                        	                                   &nbsp   
                        	                                <a class="btn btn-danger" id="res">
                        	                                	Reset</a>
                        	                          
                        	                    </div>
                        	                </div>
                        	            </div>
                        	            <div class="form-group">
                        	                <div class="col-md-4">
                        	                <label class="control-label" for="val-judul">Kategori : </label>
                        	                </div>
                        	                <div class="col-md-8">
                        	                    <input class="form-control reset" type="text" id="val-kategori" readonly>
                        	                </div>
                        	            </div>
                        	            <div class="form-group">
                        	                <div class="col-md-4">
                        	                <label class="control-label" for="harga-jual">Harga : </label>
                        	                </div>
                        	                <div class="col-md-8">
                        	                    <div class="input-group">
                        	                        <span class="input-group-addon">Rp.</span>
                        	                        <input class="form-control reset" type="text" id="harga-jual" name="harga-jual" readonly>
                        	                    </div>
                        	                    <small><span class="text-danger"><i>*) sudah termasuk diskon dan ppn 10%</i></span></small>
                        	                </div>
                        	            </div>
                        	            <div class="form-group">
                        	                <div class="col-md-4">
                        	                <label class="control-label" for="qty">Stok : </label>
                        	                </div>
                        	                <div class="col-md-3">
                        	                    <div class="input-group">
                        	                        <input class="form-control reset" type="number" id="val-stok" disabled><br>
                        	                    </div>
                        	                </div>
                        	            </div>
                        	        </div>
                        	    </div>
                        	</div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-6">
    	<div class="block">
    	    <div class="block-content block-content-full block-content-narrow">
    	<div class="panel panel-default">
    	    <div class="panel-heading">
    	        <h3 class="panel-title">
    	            <a class="accordion-toggle" data-toggle="collapse">Tracking Buku Berdasarkan Kategori</a>
    	        </h3>
    	    </div>
    	    <div id="faq1_q2" class="panel-collapse collapse in">
    	        <div class="panel-body">	
    	            <div class="row">
    	                <div class="col-lg-12">
    	                    <div class="block">
    	                        <div class="block-content block-content-full">
    	                        <div class="row">
    	                            <div class="col-lg-12">
    	                                <div class="block">
    	                                    <div class="block-content block-content-narrow">
    	                                    <form class="js-form-search form-inline" action="<?php echo base_url()?>tracking" method="post">
    	                                        <div class="form-group">
    	                                        <?php
    	                                        echo
    	                                        "<select class='js-select2 form-control' id='kat' name='kat' style='width: 250px;' data-placeholder='Pilih Kategori'>
    	                                        <option value='' disabled selected>Pilih Kategori</option>
    	                                            ";

    	                                            foreach ($kategori as $k) {  
    	                                            echo "<option value='".$k['kategori']."'>".$k['kategori']."</option>";
    	                                            }
    	                                        echo "</select>";

    	                                        ?>
    	                                        </div>
    	                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
    	                                    </form>

    	                                    <br>
    	                                    <!-- Table -->
    	                                    <div class="table-responsive push-50">
    	                                        <table class="table table-bordered">
    	                                        <thead>
    	                                            <tr>
    	                                                <th class="text-center">Kategori</th>
    	                                                <th class="text-center">Judul</th>
    	                                                <th class="text-center">Stok</th>
    	                                                <th class="text-center">Harga</th>
    	                                            </tr>
    	                                        </thead>
    	                                        <tbody>
    	                                            <?php foreach($data as $k) { ?>
    	                                            <tr>
    	
    	                                                <td class="text-center" width="50%"><?php echo $k->kategori ?></td>
    	                                                <td class="text-center" width="50%"><?php echo $k->judul ?></td>
    	                                                <td class="text-center" width="50%"><?php echo $k->stok ?></td>
    	                                                <td class="text-center" width="50%"><?php echo "Rp. ".number_format($k->harga_jual) ?></td>
    	                                            </tr>
    	                                            <?php } ?>
    	                                        </tbody>
    	                                        </table>
    	                                    </div>
    	                                    </div>
    	                                </div>
    	                            </div>
    	                        </div>
    	                        </div>
    	                    </div>
    	                </div>
    	            </div>
    	        </div>
    	    </div>
    	</div>
    	</div>
    	</div>
    </div>
    </div>
</div>
<!-- END Page Content -->

<script src="<?php echo base_url('assets/js/plugins/jscoba/dynamictable.js')?>"></script>
<script type="text/javascript">
	// show data buku
	$('#btncari').on('click',function(e) {
		e.preventDefault();
		var datas = {
			id_buku:$('#val-isbn').val()
		}
		$.post( "<?php echo base_url('tracking/getBuku')?>", datas, function( data ) {
		  $( ".result" ).html( data );
		  var hasil = $.parseJSON(data);
		  console.log(hasil);
		  $('#val-id-buku').val(hasil.id_buku);
		  $('#val-judul').val(hasil.judul);
		  $('#val-stok').val(hasil.stok);
		  $('#val-kategori').val(hasil.kategori);
		  $('#harga-jual').val(hasil.harga_jual);
		});
	});

	$('#res').on('click',function(e) {
		e.preventDefault();
		$('.reset').val('');
	});	

</script>

<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="text-center">
        OneBook by Rifqi Maulatur &copy;
    </div>
</footer>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script>
    //select2 
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        App.initHelpers('select2');
    });

</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>