<!DOCTYPE html>
 <html class="no-focus" lang="en">
    <head>
        <meta charset="utf-8">

        <title>Login OneBook</title>
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- CSS framework -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url('assets/css/style.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font.css')?>">
        <link rel="icon" href="<?php echo base_url('assets/img/icon.png')?>">
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Login Content -->
        <div class="bg-white pulldown">
            <div class="content content-boxed overflow-hidden">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                        <div class="push-30-t push-50 animated fadeIn">
                            <!-- Login Title -->
                            <div class="text-center">
                            <span class="h4 font-w600 sidebar-mini-hide"><img width="170px" height="25%" src=<?php echo base_url("assets/img/oneBook1.png") ?>></span>
                                <p class="text-muted push-15-t">LOG IN HERE</p>
                            </div>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <?php echo form_open('users/login',array('class' => 'js-validation-login form-horizontal push-30-t')); ?>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="text" id="username" name="username" value="<?php echo set_value('username') ?>">
                                            <label for="username">Username</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="password" id="password" name="password">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group push-30-t">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <button class="btn btn-sm btn-block btn-primary" type="submit">Log in</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                            <center>
                            <small>
                                <a class="font-w600" href="<?php echo base_url('tracking')?>" target="_blank">Tracking Buku Untuk Pembeli</a>
                            </small>
                            </center>
                            <!-- END Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        <!-- Login Footer -->
        <div class="pulldown push-30-t text-center animated fadeInUp">
            <small class="text-muted">OneBook by Rifqi Maulatur&copy;</small><p>
            <small>
                <a class="font-w600" href="<?php echo base_url('About')?>" target="_blank">Tentang Kami</a> |
            </small>
            <small>
                <a class="font-w600" href="<?php echo base_url('Help')?>" target="_blank">Bantuan</a>
            </small>
        </div>
        <!-- END Login Footer -->

        <!--JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="<?php echo base_url('assets/js/core/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/jquery.slimscroll.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/jquery.scrollLock.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/jquery.appear.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/jquery.countTo.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/jquery.placeholder.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/core/js.cookie.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/app.js')?>"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url('assets/js/pages/base_pages_login.js')?>"></script>
    </body>
</html>