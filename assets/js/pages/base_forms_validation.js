/*
 *  Document   : base_forms_validation.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Validation Page
 */

var BaseFormValidation = function() {
    // Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationBootstrap = function(){
        jQuery('.js-validation-bootstrap').validate({
            ignore: [],
            errorClass: 'help-block animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'val-username': {
                    required: true,
                    minlength: 3
                },
                'val-email': {
                    required: true,
                    email: true
                },
                'val-password': {
                    required: true,
                    minlength: 5
                },
                'val-confirm-password': {
                    required: true,
                    equalTo: '#val-password'
                },
                'val-select2': {
                    required: true
                },
                'val-select2-multiple': {
                    required: true,
                    minlength: 2
                },
                'val-suggestions': {
                    required: true,
                    minlength: 5
                },
                'val-skill': {
                    required: true
                },
                'val-currency': {
                    required: true,
                    currency: ['$', true]
                },
                'val-website': {
                    required: true,
                    url: true
                },
                'val-phoneus': {
                    required: true,
                    phoneUS: true
                },
                'val-digits': {
                    required: true,
                    digits: true
                },
                'val-number': {
                    required: true,
                    number: true
                },
                'val-range': {
                    required: true,
                    range: [1, 5]
                },
                'val-terms': {
                    required: true
                },
                'qty': {
                    required: true
                },
                'subtotal': {
                    required: true
                },
                'filefoto': {
                    required: true
                },
                'val-status': {
                    required: true
                },
                'val-akses': {
                    required: true
                },
                'val-judul': {
                    required: true
                },
                'val-jumlah': {
                    required: true,
                    number: true
                },
                'val-noisbn': {
                    required: true,
                    number:true
                },
                'val-penulis': {
                    required: true
                },
                'val-penerbit': {
                    required: true
                },
                'val-tahun': {
                    required: true
                },
                'val-pokok': {
                    required: true,
                    number:true
                },
                'harga-jual': {
                    required: true,
                    number: true
                },
                'val-ppn': {
                    required: true,
                    number: true
                },
                'val-diskon': {
                    required: true,
                    number: true
                },
                'val-nama-kasir': {
                    required: true
                },
                'val-nama-distributor': {
                    required: true
                },
                'val-kategori': {
                    required: true
                },
                'val-alamat': {
                    required: true
                },
                'val-telepon': {
                    required: true,
                    number:true
                }

            },
            messages: {
                'val-username': {
                    required: 'Tolong masukkan username',
                    minlength: 'Minimal 3 karakter'
                },
                'val-email': 'Please enter a valid email address',
                'val-password': {
                    required: 'Tolong masukkan passowrd',
                    minlength: 'Minimal 5 karakter'
                },
                'val-confirm-password': {
                    required: 'Tolong masukkan password',
                    minlength: 'Minimal 5 karakter',
                    equalTo: 'Password tidak cocok!'
                },
                'qty': {
                    required: 'Tolong masukkan quantity'
                },
                'subtotal': {
                    required: 'Tolong masukkan subtotal'
                },
                'filefoto': {
                    required: 'Tolong masukkan cover buku'
                },
                'val-status': {
                    required: 'Tolong masukkan status'
                },
                'val-akses': {
                    required: 'Tolong masukkan akses'
                },
                'val-judul': {
                    required: 'Tolong masukkan judul buku'
                },
                'val-jumlah': {
                    required: 'Tolong masukkan jumlah',
                    number: 'Hanya bisa dengan angka'
                },
                'val-noisbn': {
                    required: 'Tolong masukkan nomor isbn',
                    number:'Hanya bisa dengan angka'
                },
                'val-penulis': {
                    required: 'Tolong masukkan nama penuli'
                },
                'val-penerbit': {
                    required: 'Tolong masukkan nama penerbit'
                },
                'val-tahun': {
                    required: 'Tolong masukkan tahun'
                },
                'val-pokok': {
                    required: 'Tolong masukkan harga pokok',
                    number:'Hanya bisa dengan angka'
                },
                'harga-jual': {
                    required: 'Tolong masukkan harga jual',
                    number:'Hanya bisa dengan angka'
                },
                'val-ppn': {
                    required: 'Tolong masukkan ppn',
                    number:'Hanya bisa dengan angka'
                },
                'val-diskon': {
                    required: 'Tolong masukkan diskon',
                    number:'Hanya bisa dengan diskon'
                },
                'val-nama-kasir' : {
                    required:'Tolong masukkan nama kasir'
                },
                'val-nama-distributor': {
                    required: 'Tolong masukkan nama distributor'
                },
                'val-kategori': {
                    required: 'Tolong masukkan nama kategori'
                },
                'val-alamat': {
                    required: 'Tolong masukkan alamat'
                },
                'val-telepon': {
                    required: 'Tolong masukkan nomor telepon',
                    number:'Hanya bisa dengan angka'
                },
                'val-select2': 'Please select a value!',
                'val-select2-multiple': 'Please select at least 2 values!',
                'val-suggestions': 'What can we do to become better?',
                'val-skill': 'Please select a skill!',
                'val-currency': 'Tolong masukkan harga',
                'val-website': 'Please enter your website!',
                'val-phoneus': 'Please enter a US phone!',
                'val-digits': 'Please enter only digits!',
                'val-number': 'Please enter a number!',
                'val-range': 'Please enter a number between 1 and 5!',
                'val-terms': 'You must agree to the service terms!'
            }
        });
    };

    // Init Material Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationMaterial = function(){
        jQuery('.js-validation-material').validate({
            ignore: [],
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'val-username2': {
                    required: true,
                    minlength: 3
                },
                'val-email2': {
                    required: true,
                    email: true
                },
                'val-password2': {
                    required: true,
                    minlength: 5
                },
                'val-confirm-password2': {
                    required: true,
                    equalTo: '#val-password2'
                },
                'val-select22': {
                    required: true
                },
                'val-select2-multiple2': {
                    required: true,
                    minlength: 2
                },
                'val-suggestions2': {
                    required: true,
                    minlength: 5
                },
                'val-skill2': {
                    required: true
                },
                'val-currency2': {
                    required: true,
                    currency: ['$', true]
                },
                'val-website2': {
                    required: true,
                    url: true
                },
                'val-phoneus2': {
                    required: true,
                    phoneUS: true
                },
                'val-digits2': {
                    required: true,
                    digits: true
                },
                'val-number2': {
                    required: true,
                    number: true
                },
                'val-range2': {
                    required: true,
                    range: [1, 5]
                },
                'val-terms2': {
                    required: true
                }
            },
            messages: {
                'val-username2': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'val-email2': 'Please enter a valid email address',
                'val-password2': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                },
                'val-confirm-password2': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above'
                },
                'val-select22': 'Please select a value!',
                'val-select2-multiple2': 'Please select at least 2 values!',
                'val-suggestions2': 'What can we do to become better?',
                'val-skill2': 'Please select a skill!',
                'val-currency2': 'Please enter a price!',
                'val-website2': 'Please enter your website!',
                'val-phoneus2': 'Please enter a US phone!',
                'val-digits2': 'Please enter only digits!',
                'val-number2': 'Please enter a number!',
                'val-range2': 'Please enter a number between 1 and 5!',
                'val-terms2': 'You must agree to the service terms!'
            }
        });
    };

    return {
        init: function () {
            // Init Bootstrap Forms Validation
            initValidationBootstrap();

            // Init Material Forms Validation
            initValidationMaterial();

            // Init Validation on Select2 change
            jQuery('.js-select2').on('change', function(){
                jQuery(this).valid();
            });
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseFormValidation.init(); });
